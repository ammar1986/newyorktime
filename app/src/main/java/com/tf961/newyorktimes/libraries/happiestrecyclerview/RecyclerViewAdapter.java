//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.SparseArray;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.recyclerview.widget.RecyclerView.ItemDecoration;
import androidx.recyclerview.widget.RecyclerView.LayoutParams;
import androidx.recyclerview.widget.RecyclerView.OnItemTouchListener;
import androidx.recyclerview.widget.RecyclerView.State;

import java.util.ArrayList;
import java.util.List;

import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.SwipeLayout;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.implments.SwipeItemRecyclerMangerImpl;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.interfaces.SwipeAdapterInterface;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.interfaces.SwipeItemMangerInterface;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.util.Attributes.Mode;

import static android.view.View.VISIBLE;

public abstract class RecyclerViewAdapter<VH extends ViewHolder, T> extends Adapter<VH> implements StickyHeaderHandler, SwipeAdapterInterface, SwipeItemMangerInterface {
   boolean isLoadMore = false;
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_FOOTER = 2147483647;
    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;
    private SparseArray<String> mSectionsIndexer;
    private SparseArray<Integer> mAdsIndexer;
    public Activity mContext;
    private ArrayList<T> mCursor = new ArrayList();
    private ArrayList multiselect_list = new ArrayList();
    int SelectedBackroundColorID;
    MenuRecyclerListener menuRecyclerListener;
    int menuID;
    SwipeItemRecyclerMangerImpl mItemManger;
    Injection injection;
    public VH lastHeader;
    RecyclerView recyclerView;
    GridLayoutManager gridLayoutManager;
    ArrayList<RecyclerItemClickListener> onItemTouchListeners;
    boolean isMultiSelect = false;
    ActionMode mActionMode;
    private Callback mActionModeCallback;

    public void setData(ArrayList<T> objects) {
        if (objects.size() < this.mCursor.size() && this.recyclerView.endlessRecyclerOnScrollListener != null) {
            this.recyclerView.endlessRecyclerOnScrollListener.current_page = 1;
            this.recyclerView.endlessRecyclerOnScrollListener.previousTotal = 0;
            this.recyclerView.endlessRecyclerOnScrollListener.moreDataAvailable = false;
        }

        this.mCursor = objects;
        if (this.isSection()) {
            this.calculateSectionHeaders();
        } else if (this.injection != null) {
            this.calculateAdsSections();
        }

        this.notifyDataSetChanged();
        if(recyclerView!=null)
        this.recyclerView.setIsRefreshing(false);
    }

    public void refreshData(){
        if (this.isSection()) {
            this.calculateSectionHeaders();
        } else if (this.injection != null) {
            this.calculateAdsSections();
        }
        notifyDataSetChanged();
    }

    public void addData(ArrayList<T> object) {
        if(this.recyclerView != null)
            if (this.recyclerView.endlessRecyclerOnScrollListener != null) {
                this.recyclerView.endlessRecyclerOnScrollListener.moreDataAvailable = false;
            }

        this.getData().addAll(object);
        if (this.isSection()) {
            this.calculateSectionHeaders();
        } else if (this.injection != null) {
            this.calculateAdsSections();
        }
        notifyDataSetChanged();
    }
    public void addData(T object) {
            if(this.recyclerView != null)
        if (this.recyclerView.endlessRecyclerOnScrollListener != null) {
            this.recyclerView.endlessRecyclerOnScrollListener.moreDataAvailable = false;
        }

        this.getData().add(object);
        if (this.isSection()) {
            this.calculateSectionHeaders();
        } else if (this.injection != null) {
            this.calculateAdsSections();
        }
        notifyItemInserted(getData().size() + mSectionsIndexer.size()  + mAdsIndexer.size() -1);
    }

    public void inject(Injection injection) {
        this.injection = injection;
    }

    public ArrayList<T> getData() {
        return this.mCursor;
    }

    public ArrayList<T> getMultiSelectedList() {
        return this.multiselect_list;
    }

    public RecyclerViewAdapter(Activity context) {
        this.mActionModeCallback = new NamelessClass_1();
        this.mContext = context;
        this.mSectionsIndexer = new SparseArray();
        this.mAdsIndexer = new SparseArray();
        this.onItemTouchListeners = new ArrayList();
    }

    protected void continu(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        if (this.isStickyHeader()) {
            StickyLayoutManager stickyLayoutManager = new StickyLayoutManager(this.mContext, this.getOrientation(), false, this);
            stickyLayoutManager.elevateHeaders(true);
            recyclerView.getRecyclerView().setLayoutManager(stickyLayoutManager);
        } else if (this.gridLayoutManager == null) {
            LinearLayoutManager linearLayoutmanager = new LinearLayoutManager(this.mContext, this.getOrientation(), false);
            recyclerView.getRecyclerView().setLayoutManager(linearLayoutmanager);
        } else {
            recyclerView.getRecyclerView().setLayoutManager(this.gridLayoutManager);
        }

        if (isLoadMore) {
            recyclerView.addEndlessRecyclerOnScrollListener(this.mContext);
        }

        this.mItemManger = new SwipeItemRecyclerMangerImpl(this);
    }

    class NamelessClass_1 implements Callback {
        NamelessClass_1() {
        }

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            if (RecyclerViewAdapter.this.menuID != 0) {
                inflater.inflate(RecyclerViewAdapter.this.menuID, menu);
            }

            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            if (RecyclerViewAdapter.this.menuRecyclerListener != null) {
                RecyclerViewAdapter.this.menuRecyclerListener.onMenuItemSelected(item.getItemId());
            }

            if (mode != null) {
                mode.finish();
            }

            return false;
        }

        public void onDestroyActionMode(ActionMode mode) {
            RecyclerViewAdapter.this.mActionMode = null;
            RecyclerViewAdapter.this.multiselect_list = new ArrayList();
            RecyclerViewAdapter.this.notifyDataSetChanged();
            (new Handler()).postDelayed(new Runnable() {
                public void run() {
                    RecyclerViewAdapter.this.isMultiSelect = false;
                }
            }, 200L);
        }
    }
    public RecyclerViewAdapter(Activity context, GridLayoutManager gridLayoutManager) {


        this.mActionModeCallback = new NamelessClass_1();
        this.mContext = context;
        this.mSectionsIndexer = new SparseArray();
        this.mAdsIndexer = new SparseArray();
        this.onItemTouchListeners = new ArrayList();
        this.gridLayoutManager = gridLayoutManager;
    }

    public abstract VH getFooterHolder(ViewGroup var1);

    public AdsHolder getAdsHolder(View v) {
        return new AdsHolder(v);
    }

    public int getItemCount() {
        if (this.mCursor != null) {
            return this.getData() == null ? 0 : this.getData().size() + this.mSectionsIndexer.size() + this.mAdsIndexer.size() + (isLoadMore ? 1 : 0);
        } else {
            return 0;
        }
    }



    public abstract boolean attachAlwaysLastHeader();

    public abstract int getOrientation();

    public abstract HeaderObject getSectionCondition(int var1);

    public abstract boolean isSection();

    public abstract boolean isStickyHeader();

    public abstract int getItemType(int var1);

    public abstract int getHeaderLayout();

    private void calculateSectionHeaders() {
        this.mSectionsIndexer.clear();
        this.mAdsIndexer.clear();
        int i = 0;
        String previous = "";
        int count = 0;
        int adCounts = 0;
        ArrayList<T> c = this.getData();
        if (c != null) {
            String temp;
            boolean lastMessageisForceHeader = false;
            int j;
            if (this.injection != null) {
                for(j = 0; j < c.size() + ((c.size() - this.injection.getStarting()) % this.injection.getConcurrency() == 0 ? 1 : this.injection.getConcurrency()); ++j) {
                    if (j == this.injection.getStarting() + 1 || j > this.injection.getStarting() + 1 && (j - this.injection.getStarting()) % this.injection.getConcurrency() == 0) {
                        this.mAdsIndexer.put(i + count + adCounts, (Integer)this.injection.getViewTypes().get(this.mAdsIndexer.size() % this.injection.getViewTypes().size()) * -1);
                        ++adCounts;
                    }

                    temp = this.getSectionCondition(j).title;
                    if (temp == null || !previous.equals(temp) || getSectionCondition(j).isNewHeader || lastMessageisForceHeader) {
                        this.mSectionsIndexer.put(i + count + adCounts, temp);
                        previous = temp;
                        ++count;
                    }
                    lastMessageisForceHeader = this.getSectionCondition(j).isNewHeader;
                    ++i;
                }
            } else {
                for(j = 0; j < c.size(); ++j) {
                    temp = this.getSectionCondition(j).title;
                    if (temp == null || !previous.equals(temp)|| getSectionCondition(j).isNewHeader || lastMessageisForceHeader) {
                        this.mSectionsIndexer.put(i + count + adCounts, temp);
                        previous = temp;
                        ++count;
                        lastMessageisForceHeader = this.getSectionCondition(j).isNewHeader;
                    }

                    ++i;
                }
            }

        }
    }

    private void calculateAdsSections() {
        this.mSectionsIndexer.clear();
        this.mAdsIndexer.clear();
        int i = 0;
        int count = 0;
        ArrayList<T> c = this.getData();
        if (c != null) {
            if (this.injection != null) {
                for(int j = 0; j < c.size() + (c.size() % this.injection.getConcurrency() == 0 ? 1 : 0); ++j) {
                    if (j > this.injection.getStarting() && j % this.injection.getConcurrency() == 0) {
                        this.mAdsIndexer.put(i + count, (Integer)this.injection.getViewTypes().get(this.mAdsIndexer.size() % this.injection.getViewTypes().size()) * -1);
                        ++count;
                    }

                    ++i;
                }
            }

        }
    }

    private int countNumberAdsBefore(int position) {
        int count = 0;

        for(int i = 0; i < this.mAdsIndexer.size() && position > this.mAdsIndexer.keyAt(i); ++i) {
            ++count;
        }

        return count;
    }

    private int countNumberSectionsBefore(int position) {
        int count = 0;

        for(int i = 0; i < this.mSectionsIndexer.size() && position > this.mSectionsIndexer.keyAt(i); ++i) {
            ++count;
        }

        return count;
    }

    private int countNumberSectionsAndAdsBefore(int position) {
        int count = 0;

        int i;
        for(i = 0; i < this.mSectionsIndexer.size() && position > this.mSectionsIndexer.keyAt(i); ++i) {
            ++count;
        }

        for(i = 0; i < this.mAdsIndexer.size() && position > this.mAdsIndexer.keyAt(i); ++i) {
            ++count;
        }

        return count;
    }

    public void removeAllTouchListeners() {
        for(int i = 0; i < this.onItemTouchListeners.size(); ++i) {
            this.recyclerView.getRecyclerView().removeOnItemTouchListener((OnItemTouchListener)this.onItemTouchListeners.get(i));
        }

        this.onItemTouchListeners.clear();
    }

    public void removeTouchListeners(RecyclerItemClickListener recyclerItemClickListener) {
        this.recyclerView.getRecyclerView().removeOnItemTouchListener(recyclerItemClickListener);
        this.onItemTouchListeners.remove(recyclerItemClickListener);
    }

    public void setOnItemClickedListener(Context context, int SelectedBackroundColorID, final OnItemClickListener onItemClickListener) {
        this.SelectedBackroundColorID = SelectedBackroundColorID;
        RecyclerItemClickListener recyclerItemClickListener = new RecyclerItemClickListener(context, this.recyclerView.getRecyclerView(), new com.tf961.newyorktimes.libraries.happiestrecyclerview.RecyclerItemClickListener.OnItemClickListener() {
            public void onItemClick(View view, int position) {
                if (!RecyclerViewAdapter.this.isMultiSelect) {
                    if (RecyclerViewAdapter.this.mSectionsIndexer.indexOfKey(position) >= 0) {
                        return;
                    }

                    if (RecyclerViewAdapter.this.mAdsIndexer.indexOfKey(position) >= 0) {
                        return;
                    }

                    onItemClickListener.onItemClick(view, position - RecyclerViewAdapter.this.countNumberSectionsAndAdsBefore(position));
                }

            }

            public void onItemLongClick(View view, int position) {
                if (!RecyclerViewAdapter.this.isMultiSelect) {
                    if (RecyclerViewAdapter.this.mSectionsIndexer.indexOfKey(position) >= 0) {
                        return;
                    }

                    if (RecyclerViewAdapter.this.mAdsIndexer.indexOfKey(position) >= 0) {
                        return;
                    }

                    onItemClickListener.onItemLongClick(view, position - RecyclerViewAdapter.this.countNumberSectionsAndAdsBefore(position));
                }

            }
        });
        this.onItemTouchListeners.add(recyclerItemClickListener);
        this.recyclerView.getRecyclerView().addOnItemTouchListener(recyclerItemClickListener);
    }

    public void enableMultiSelection(final Context context, int SelectedBackroundColorID, MenuRecyclerListener menuRecyclerListener, int menuID) {
        this.menuID = menuID;
        this.SelectedBackroundColorID = SelectedBackroundColorID;
        this.menuRecyclerListener = menuRecyclerListener;
        this.recyclerView.getRecyclerView().addOnItemTouchListener(new RecyclerItemClickListener(context, this.recyclerView.getRecyclerView(), new com.tf961.newyorktimes.libraries.happiestrecyclerview.RecyclerItemClickListener.OnItemClickListener() {
            public void onItemClick(View view, int position) {
                if (RecyclerViewAdapter.this.mSectionsIndexer.indexOfKey(position) < 0) {
                    if (RecyclerViewAdapter.this.mAdsIndexer.indexOfKey(position) < 0) {
                        if (RecyclerViewAdapter.this.isMultiSelect) {
                            RecyclerViewAdapter.this.multi_select(position - RecyclerViewAdapter.this.countNumberSectionsAndAdsBefore(position));
                        } else {
                            Toast.makeText(context.getApplicationContext(), "Details Page", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            }

            public void onItemLongClick(View view, int position) {
                if (RecyclerViewAdapter.this.mSectionsIndexer.indexOfKey(position) < 0) {
                    if (RecyclerViewAdapter.this.mAdsIndexer.indexOfKey(position) < 0) {
                        if (!RecyclerViewAdapter.this.isMultiSelect) {
                            RecyclerViewAdapter.this.multiselect_list = new ArrayList();
                            RecyclerViewAdapter.this.isMultiSelect = true;
                            if (RecyclerViewAdapter.this.mActionMode == null) {
                                RecyclerViewAdapter.this.mActionMode = RecyclerViewAdapter.this.recyclerView.startActionMode(RecyclerViewAdapter.this.mActionModeCallback);
                            }
                        }

                        RecyclerViewAdapter.this.multi_select(position - RecyclerViewAdapter.this.countNumberSectionsAndAdsBefore(position));
                    }
                }
            }
        }));
    }

    void multi_select(int position) {
        if (!(this.recyclerView.getRecyclerView().getAdapter() instanceof CursorRecyclerViewAdapter) && this.recyclerView.getRecyclerView().getAdapter() instanceof RecyclerViewAdapter && this.mActionMode != null) {
            if (this.multiselect_list.contains(this.getData().get(position))) {
                this.multiselect_list.remove(this.getData().get(position));
            } else {
                this.multiselect_list.add(this.getData().get(position));
            }

            if (this.multiselect_list.size() > 0) {
                this.mActionMode.setTitle("" + this.multiselect_list.size());
            } else {
                this.mActionMode.setTitle("");
            }

            this.notifyDataSetChanged();
        }

    }

    public SparseArray<String> getAdapterData() {
        return this.mSectionsIndexer.size() == 0 ? null : this.mSectionsIndexer;
    }

    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(true);
    }

    public abstract void onBindViewHolders(VH var1, int var2);

    public void onBindViewHolder(VH viewHolder, int position) {

        if (!isSection()) {
            if (getItemViewType(position) == TYPE_FOOTER) {
                if (recyclerView.endlessRecyclerOnScrollListener != null && recyclerView.endlessRecyclerOnScrollListener.loading && recyclerView.endlessRecyclerOnScrollListener.moreDataAvailable)
                     viewHolder.setVisibility(true);
                else viewHolder.setVisibility(false);
            } else if (getItemViewType(position) < 0) {
                int pos = (countNumberAdsBefore(position)) % injection.getViewTypes().size();
                injection.onBindViewHolder(viewHolder, (pos == 0 ? injection.getViewTypes().get(injection.getViewTypes().size() - 1) : pos));
            } else {
                viewHolder.setItemPosition(position - countNumberSectionsAndAdsBefore(position));
                onBindViewHolders(viewHolder, position - countNumberSectionsAndAdsBefore(position));
                if (getMultiSelectedList().contains(this.getData().get(position - countNumberSectionsAndAdsBefore(position)))) {
                    viewHolder.itemView.setBackgroundColor(SelectedBackroundColorID);
                } else {
                    viewHolder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
                }
            }

        } else {
            if (getItemViewType(position) == TYPE_FOOTER) {
                if (recyclerView.endlessRecyclerOnScrollListener != null && recyclerView.endlessRecyclerOnScrollListener.loading && recyclerView.endlessRecyclerOnScrollListener.moreDataAvailable)
                     viewHolder.setVisibility(true);
                else  viewHolder.setVisibility(false);
            } else if (getItemViewType(position) == TYPE_HEADER) {
                createHeaderViewHolder(viewHolder, position - countNumberSectionsAndAdsBefore(position));
            } else if (getItemViewType(position) < 0) {
                int pos = (countNumberAdsBefore(position)) % injection.getViewTypes().size();
                injection.onBindViewHolder(viewHolder, (pos == 0 ? injection.getViewTypes().get(injection.getViewTypes().size() - 1) : pos));
            } else {
                viewHolder.setItemPosition(position - countNumberSectionsAndAdsBefore(position));
                onBindViewHolders(viewHolder, position - countNumberSectionsAndAdsBefore(position));
                if (getMultiSelectedList().contains(this.getData().get(position - countNumberSectionsAndAdsBefore(position)))) {
                    viewHolder.itemView.setBackgroundColor(SelectedBackroundColorID);
                } else {
                    viewHolder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
                }
            }
        }

    }

    public abstract ViewHolder onCreateViewHolders(ViewGroup var1, int var2);

    /** @deprecated */
    @Deprecated
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = null;
        if (viewType < 0) {
            return (VH) this.injection.onCreateViewHolder(parent, viewType * -1);
        } else {
            switch(viewType) {
                case 0:
                    return (VH) this.onCreateViewHolders(parent, 0);
                case 2147483647:
                    return this.getFooterHolder(parent);
                default:
                    return (VH) this.onCreateViewHolders(parent, viewType);
            }
        }
    }

    public abstract void bindHeaderViewHolder(VH var1, int var2);

    private void createHeaderViewHolder(VH headerHolder, int position) {
        if (position >= 0) {
            String date = this.getSectionCondition(position).title;
            if (this.attachAlwaysLastHeader()) {
                if (date.equals(this.getLastHeaderData())) {
                    if (((StickyLayoutManager)this.recyclerView.getRecyclerView().getLayoutManager()).findFirstVisibleItemPosition() > headerHolder.getAdapterPosition()) {
                        headerHolder.itemView.setVisibility(View.GONE);
                    } else {
                        headerHolder.itemView.setVisibility(VISIBLE);
                    }

                    this.lastHeader = headerHolder;
                } else {
                    headerHolder.itemView.setVisibility(VISIBLE);
                    if (this.lastHeader != null) {
                        headerHolder.itemView.setVisibility(VISIBLE);
                    }
                }
            }

            this.bindHeaderViewHolder(headerHolder, position);
        }
    }

    public String getLastHeaderData() {
        return (String)this.mSectionsIndexer.get(this.mSectionsIndexer.keyAt(this.mSectionsIndexer.size() - 1));
    }

    public int getItemViewType(int position) {
        if (position == this.getItemCount() - 1 && isLoadMore) {
            return 2147483647;
        } else if (!this.isSection() && this.injection == null) {
            return this.getItemType(position);
        } else if (this.mSectionsIndexer.indexOfKey(position) >= 0) {
            return 0;
        } else {
            return this.mAdsIndexer.indexOfKey(position) >= 0 ? (Integer)this.mAdsIndexer.get(position) : this.getItemType(position - this.countNumberSectionsAndAdsBefore(position));
        }
    }

    public int getAdapterPosition(ViewHolder viewHolder) {
        int position = viewHolder.getAdapterPosition();
        return position - this.countNumberSectionsAndAdsBefore(position);
    }

    public void openItem(int position) {
        this.mItemManger.openItem(position);
    }

    public void closeItem(int position) {
        this.mItemManger.closeItem(position);
    }

    public void closeAllExcept(SwipeLayout layout) {
        this.mItemManger.closeAllExcept(layout);
    }

    public void closeAllItems() {
        this.mItemManger.closeAllItems();
    }

    public List<Integer> getOpenItems() {
        return this.mItemManger.getOpenItems();
    }

    public List<SwipeLayout> getOpenLayouts() {
        return this.mItemManger.getOpenLayouts();
    }

    public void removeShownLayouts(SwipeLayout layout) {
        this.mItemManger.removeShownLayouts(layout);
    }

    public boolean isOpen(int position) {
        return this.mItemManger.isOpen(position);
    }

    public Mode getMode() {
        return this.mItemManger.getMode();
    }

    public void setMode(Mode mode) {
        this.mItemManger.setMode(mode);
    }

    public void onSwipeClicked(SwipeLayout swipeLayout) {
        this.mItemManger.removeShownLayouts(swipeLayout);
        this.mItemManger.closeAllItems();
    }

    protected RecyclerViewAdapter<VH, T>.CustomDividerItemDecoration getCustomDividerItemDecoration(Drawable divider, boolean includeAds, boolean includeSections) {
        return new RecyclerViewAdapter.CustomDividerItemDecoration(divider, includeAds, includeSections);
    }

    private class CustomDividerItemDecoration extends ItemDecoration {
        private Drawable mDivider;
        private boolean includeAds;
        private boolean includeSections;

        private CustomDividerItemDecoration(Drawable divider, boolean includeAds, boolean includeSections) {
            this.mDivider = divider;
            this.includeAds = includeAds;
            this.includeSections = includeSections;
        }

        public void onDraw(Canvas canvas, androidx.recyclerview.widget.RecyclerView parent, State state) {
            int dividerLeft = parent.getPaddingLeft();
            int dividerRight = parent.getWidth() - parent.getPaddingRight();
            int childCount = parent.getChildCount();

            for(int i = 0; i <= childCount - (isLoadMore ? 2 : 1); ++i) {
                View child = parent.getChildAt(i);
                if ((this.includeSections || RecyclerViewAdapter.this.mSectionsIndexer.indexOfKey(i) <= 0) && (this.includeAds || RecyclerViewAdapter.this.mAdsIndexer.indexOfKey(i) <= 0)) {
                    LayoutParams params = (LayoutParams)child.getLayoutParams();
                    int dividerTop = child.getBottom() + params.bottomMargin;
                    int dividerBottom = dividerTop + this.mDivider.getIntrinsicHeight();
                    this.mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
                    this.mDivider.draw(canvas);
                }
            }

        }
    }
}
