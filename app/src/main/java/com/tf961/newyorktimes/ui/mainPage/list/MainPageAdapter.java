package com.tf961.newyorktimes.ui.mainPage.list;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.tf961.newyorktimes.R;
import com.tf961.newyorktimes.data.model.api.mainPage.NYTimeInterface;
import com.tf961.newyorktimes.databinding.NyTimeRowBinding;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.ViewHolder;
import com.tf961.newyorktimes.base.BaseAdapter;
import com.tf961.newyorktimes.base.BaseViewHolder;

/**
 * Created by appsassaad on 5/18/2018.
 */

public class MainPageAdapter extends BaseAdapter<BaseViewHolder<ViewDataBinding, NYTimeInterface>, NYTimeInterface> {

    public MainPageAdapter(Activity context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolders(ViewGroup parent, int viewType) {
        NyTimeRowBinding nyTimeRowBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.ny_time_row, parent, false);
        return new MainPageVH(nyTimeRowBinding);
    }

}
