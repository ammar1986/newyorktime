package com.tf961.newyorktimes.data.model.api;

import com.google.gson.annotations.SerializedName;

import org.parceler.Generated;
import org.parceler.Parcel;

@Generated("org.jsonschema2pojo")
@Parcel(Parcel.Serialization.VALUE)
public class BaseResponse<T> {
    @SerializedName(value = "status", alternate = "Status")
    private String status;
    @SerializedName(value = "copyright", alternate = "Copyright")
    private String message;
    @SerializedName(value = "results", alternate = "Results")
    private T data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}