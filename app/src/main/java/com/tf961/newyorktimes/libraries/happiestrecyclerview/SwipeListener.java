//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

public interface SwipeListener {
    void onSwipe();

    void onSwipeConnectionError();

    void loadMore(int var1);
}
