package com.tf961.newyorktimes.base;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.tf961.newyorktimes.libraries.happiestrecyclerview.HeaderObject;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.RecyclerViewAdapter;

/**
 * Created by appsassaad on 5/18/2018.
 */

public abstract class BaseAdapter<T extends BaseViewHolder, V> extends RecyclerViewAdapter<T, V> {

    public BaseAdapter(Activity context) {
        super(context);
    }

    @Override
    public T getFooterHolder(ViewGroup var1) {
        return null;
    }

    @Override
    public boolean attachAlwaysLastHeader() {
        return false;
    }

    @Override
    public void onBindViewHolders(T viewHolder, int position) {
        viewHolder.setData(getData().get(position));
    }

    @Override
    public void bindHeaderViewHolder(T var1, int var2) {

    }

    @Override
    public int getHeaderLayout() {
        return 0;
    }

    @Override
    public boolean isStickyHeader() {
        return false;
    }

    @Override
    public int getItemType(int i) {
        return 1;
    }

    @Override
    public int getOrientation() {
        return LinearLayout.VERTICAL;
    }

    @Override
    public HeaderObject getSectionCondition(int var1) {
        return null;
    }

    @Override
    public boolean isSection() {
        return false;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return 0;
    }



}
