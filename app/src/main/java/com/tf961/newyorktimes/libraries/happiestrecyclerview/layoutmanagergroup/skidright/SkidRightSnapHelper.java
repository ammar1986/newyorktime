//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview.layoutmanagergroup.skidright;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView.LayoutManager;
import androidx.recyclerview.widget.SnapHelper;

public class SkidRightSnapHelper extends SnapHelper {
    private int mDirection;

    public SkidRightSnapHelper() {
    }

    public int[] calculateDistanceToFinalSnap(@NonNull LayoutManager layoutManager, @NonNull View targetView) {
        if (layoutManager instanceof LayoutManagerSkidRight) {
            int[] out = new int[2];
            if (layoutManager.canScrollHorizontally()) {
                out[0] = ((LayoutManagerSkidRight)layoutManager).calculateDistanceToPosition(layoutManager.getPosition(targetView));
                out[1] = 0;
            } else {
                out[0] = 0;
                out[1] = ((LayoutManagerSkidRight)layoutManager).calculateDistanceToPosition(layoutManager.getPosition(targetView));
            }

            return out;
        } else {
            return null;
        }
    }

    public int findTargetSnapPosition(LayoutManager layoutManager, int velocityX, int velocityY) {
        if (layoutManager.canScrollHorizontally()) {
            this.mDirection = velocityX;
        } else {
            this.mDirection = velocityY;
        }

        return -1;
    }

    public View findSnapView(LayoutManager layoutManager) {
        if (layoutManager instanceof LayoutManagerSkidRight) {
            int pos = ((LayoutManagerSkidRight)layoutManager).getFixedScrollPosition(this.mDirection, this.mDirection != 0 ? 0.8F : 0.5F);
            this.mDirection = 0;
            if (pos != -1) {
                return layoutManager.findViewByPosition(pos);
            }
        }

        return null;
    }
}
