package com.tf961.newyorktimes.ui.mainPage.list;

import com.tf961.newyorktimes.data.model.api.mainPage.NYTimeInterface;
import com.tf961.newyorktimes.databinding.NyTimeRowBinding;
import com.tf961.newyorktimes.base.BaseViewHolder;


/**
 * Created by ama on 2/23/2016.
 */

public class  MainPageVH extends BaseViewHolder<NyTimeRowBinding, NYTimeInterface> {

    public MainPageVH(NyTimeRowBinding binding) {
        super(binding);
    }

    public NyTimeRowBinding getBinding() {
        return mViewDataBinding;
    }

}