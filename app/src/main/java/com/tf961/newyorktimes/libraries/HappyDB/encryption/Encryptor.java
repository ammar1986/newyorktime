//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.HappyDB.encryption;

import org.json.JSONObject;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Encryptor {
    public static final String EncryktionKey = "3ZLRCTaW8bc9d52W";

    public Encryptor() {
    }

    public static String encrypt(String seed, String cleartext) throws Exception {
        byte[] rawKey = getRawKey(seed.getBytes());
        byte[] result = encrypt(rawKey, cleartext.getBytes("UTF-8"));
        return toBase64(result);
    }

    public static String decrypt(String seed, String encrypted) throws Exception {
        String dataReturned = "";

        byte[] rawKey;
        try {
             rawKey = getRawKey(seed.getBytes("UTF-8"));
            byte[] enc = fromBase64(encrypted);
            rawKey = decrypt(rawKey, enc);
            dataReturned = new String(rawKey);
        } catch (Exception var10) {
            var10.printStackTrace();

            try {
                String seedStatic = "3ZLRCTaW8bc9d52W";
                rawKey = getRawKey(seedStatic.getBytes("UTF-8"));
                byte[] enc = fromBase64(encrypted);
                byte[] result = decrypt(rawKey, enc);
                dataReturned = new String(result);
            } catch (Exception var9) {
                var9.printStackTrace();
                JSONObject statuscode = new JSONObject();
                JSONObject code = new JSONObject();
                code.put("code", 9999);
                code.put("message", "EnxDec Error");
                statuscode.put("statusCode", code);

                try {
                    dataReturned = statuscode.toString();
                } catch (Exception var8) {
                    dataReturned = "0";
                }
            }
        }

        return dataReturned;
    }

    private static byte[] getRawKey(byte[] seed) throws Exception {
        SecretKey skey = new SecretKeySpec(seed, "AES");
        byte[] raw = skey.getEncoded();
        return raw;
    }

    private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(raw);
        cipher.init(1, skeySpec, ivParameterSpec);
        byte[] encrypted = cipher.doFinal(clear);
        return encrypted;
    }

    private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(raw);
        cipher.init(2, skeySpec, ivParameterSpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }

    public static String toBase64(byte[] in) {
        return b64.encode1(in);
    }

    public static byte[] fromBase64(String str) throws Exception {
        return b64.decode1(str);
    }
}
