//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview.layoutmanagergroup.picker;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Recycler;
import androidx.recyclerview.widget.RecyclerView.State;

public class LayoutManagerPicker extends LinearLayoutManager {
    private static final String TAG = "PickerLayoutManager";
    private float mScale = 0.5F;
    private boolean mIsAlpha = true;
    private LinearSnapHelper mLinearSnapHelper = new LinearSnapHelper();
    private LayoutManagerPicker.OnSelectedViewListener mOnSelectedViewListener;
    private int mItemViewWidth;
    private int mItemViewHeight;
    private int mItemCount = -1;
    private RecyclerView mRecyclerView;
    private int mOrientation;

    public LayoutManagerPicker(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
        this.mOrientation = orientation;
    }

    public LayoutManagerPicker(Context context, RecyclerView recyclerView, int orientation, boolean reverseLayout, int itemCount, float scale, boolean isAlpha) {
        super(context, orientation, reverseLayout);
        this.mItemCount = itemCount;
        this.mOrientation = orientation;
        this.mRecyclerView = recyclerView;
        this.mIsAlpha = isAlpha;
        this.mScale = scale;
        if (this.mItemCount != 0) {
            this.setAutoMeasureEnabled(false);
        }

    }

    public void onAttachedToWindow(RecyclerView view) {
        super.onAttachedToWindow(view);
        this.mLinearSnapHelper.attachToRecyclerView(view);
    }

    public void onMeasure(Recycler recycler, State state, int widthSpec, int heightSpec) {
        if (this.getItemCount() != 0 && this.mItemCount != 0) {
            View view = recycler.getViewForPosition(0);
            this.measureChildWithMargins(view, widthSpec, heightSpec);
            this.mItemViewWidth = view.getMeasuredWidth();
            this.mItemViewHeight = view.getMeasuredHeight();
            int paddingVertical;
            if (this.mOrientation == 0) {
                paddingVertical = (this.mItemCount - 1) / 2 * this.mItemViewWidth;
                this.mRecyclerView.setClipToPadding(false);
                this.mRecyclerView.setPadding(paddingVertical, 0, paddingVertical, 0);
                this.setMeasuredDimension(this.mItemViewWidth * this.mItemCount, this.mItemViewHeight);
            } else if (this.mOrientation == 1) {
                paddingVertical = (this.mItemCount - 1) / 2 * this.mItemViewHeight;
                this.mRecyclerView.setClipToPadding(false);
                this.mRecyclerView.setPadding(0, paddingVertical, 0, paddingVertical);
                this.setMeasuredDimension(this.mItemViewWidth, this.mItemViewHeight * this.mItemCount);
            }
        } else {
            super.onMeasure(recycler, state, widthSpec, heightSpec);
        }

    }

    public void onLayoutChildren(Recycler recycler, State state) {
        super.onLayoutChildren(recycler, state);
        if (this.getItemCount() >= 0 && !state.isPreLayout()) {
            if (this.mOrientation == 0) {
                this.scaleHorizontalChildView();
            } else if (this.mOrientation == 1) {
                this.scaleVerticalChildView();
            }

        }
    }

    public int scrollHorizontallyBy(int dx, Recycler recycler, State state) {
        this.scaleHorizontalChildView();
        return super.scrollHorizontallyBy(dx, recycler, state);
    }

    public int scrollVerticallyBy(int dy, Recycler recycler, State state) {
        this.scaleVerticalChildView();
        return super.scrollVerticallyBy(dy, recycler, state);
    }

    private void scaleHorizontalChildView() {
        float mid = (float)this.getWidth() / 2.0F;

        for(int i = 0; i < this.getChildCount(); ++i) {
            View child = this.getChildAt(i);
            float childMid = (float)(this.getDecoratedLeft(child) + this.getDecoratedRight(child)) / 2.0F;
            float scale = 1.0F + -1.0F * (1.0F - this.mScale) * Math.min(mid, Math.abs(mid - childMid)) / mid;
            child.setScaleX(scale);
            child.setScaleY(scale);
            if (this.mIsAlpha) {
                child.setAlpha(scale);
            }
        }

    }

    private void scaleVerticalChildView() {
        float mid = (float)this.getHeight() / 2.0F;

        for(int i = 0; i < this.getChildCount(); ++i) {
            View child = this.getChildAt(i);
            float childMid = (float)(this.getDecoratedTop(child) + this.getDecoratedBottom(child)) / 2.0F;
            float scale = 1.0F + -1.0F * (1.0F - this.mScale) * Math.min(mid, Math.abs(mid - childMid)) / mid;
            child.setScaleX(scale);
            child.setScaleY(scale);
            if (this.mIsAlpha) {
                child.setAlpha(scale);
            }
        }

    }

    public void onScrollStateChanged(int state) {
        super.onScrollStateChanged(state);
        if (state == 0 && this.mOnSelectedViewListener != null && this.mLinearSnapHelper != null) {
            View view = this.mLinearSnapHelper.findSnapView(this);
            int position = this.getPosition(view);
            this.mOnSelectedViewListener.onSelectedView(view, position);
        }

    }

    public void setOnSelectedViewListener(LayoutManagerPicker.OnSelectedViewListener listener) {
        this.mOnSelectedViewListener = listener;
    }

    public interface OnSelectedViewListener {
        void onSelectedView(View var1, int var2);
    }
}
