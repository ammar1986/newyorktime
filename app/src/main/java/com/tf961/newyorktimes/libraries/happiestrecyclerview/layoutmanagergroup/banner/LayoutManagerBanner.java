//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview.layoutmanagergroup.banner;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.LayoutParams;
import androidx.recyclerview.widget.RecyclerView.Recycler;
import androidx.recyclerview.widget.RecyclerView.State;

import java.lang.ref.WeakReference;

public class LayoutManagerBanner extends LinearLayoutManager {
    private static final String TAG = "BannerLayoutManager";
    private LinearSnapHelper mLinearSnapHelper = new LinearSnapHelper();
    private RecyclerView mRecyclerView;
    private LayoutManagerBanner.OnSelectedViewListener mOnSelectedViewListener;
    private int mRealCount;
    private int mCurrentPosition = 0;
    private LayoutManagerBanner.TaskHandler mHandler;
    private long mTimeDelayed = 1000L;
    private int mOrientation;
    private float mTimeSmooth = 150.0F;

    public LayoutManagerBanner(Context context, RecyclerView recyclerView, int realCount) {
        super(context);
        this.mRealCount = realCount;
        this.mHandler = new LayoutManagerBanner.TaskHandler(this);
        this.mRecyclerView = recyclerView;
        this.setOrientation(RecyclerView.HORIZONTAL);
        this.mOrientation = 0;
    }

    public LayoutManagerBanner(Context context, RecyclerView recyclerView, int realCount, int orientation) {
        super(context);
        this.mRealCount = realCount;
        this.mHandler = new LayoutManagerBanner.TaskHandler(this);
        this.mRecyclerView = recyclerView;
        this.setOrientation(orientation);
        this.mOrientation = orientation;
    }

    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2);
    }

    public void onAttachedToWindow(RecyclerView view) {
        super.onAttachedToWindow(view);
        this.mLinearSnapHelper.attachToRecyclerView(view);
    }

    public void smoothScrollToPosition(RecyclerView recyclerView, State state, int position) {
        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(recyclerView.getContext()) {
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return LayoutManagerBanner.this.mTimeSmooth / (float)displayMetrics.densityDpi;
            }
        };
        smoothScroller.setTargetPosition(position);
        this.startSmoothScroll(smoothScroller);
    }

    public void onScrollStateChanged(int state) {
        super.onScrollStateChanged(state);
        if (state == 0) {
            if (this.mLinearSnapHelper != null) {
                View view = this.mLinearSnapHelper.findSnapView(this);
                this.mCurrentPosition = this.getPosition(view);
                if (this.mOnSelectedViewListener != null) {
                    this.mOnSelectedViewListener.onSelectedView(view, this.mCurrentPosition % this.mRealCount);
                }

                this.mHandler.setSendMsg(true);
                Message msg = Message.obtain();
                ++this.mCurrentPosition;
                msg.what = this.mCurrentPosition;
                this.mHandler.sendMessageDelayed(msg, this.mTimeDelayed);
            }
        } else if (state == 1) {
            this.mHandler.setSendMsg(false);
        }

    }

    public void setTimeDelayed(long timeDelayed) {
        this.mTimeDelayed = timeDelayed;
    }

    public void setTimeSmooth(float timeSmooth) {
        this.mTimeSmooth = timeSmooth;
    }

    public void onLayoutChildren(Recycler recycler, State state) {
        super.onLayoutChildren(recycler, state);
        this.mHandler.setSendMsg(true);
        Message msg = Message.obtain();
        msg.what = this.mCurrentPosition + 1;
        this.mHandler.sendMessageDelayed(msg, this.mTimeDelayed);
    }

    public void setOnSelectedViewListener(LayoutManagerBanner.OnSelectedViewListener listener) {
        this.mOnSelectedViewListener = listener;
    }

    public RecyclerView getRecyclerView() {
        return this.mRecyclerView;
    }

    private static class TaskHandler extends Handler {
        private WeakReference<LayoutManagerBanner> mWeakBanner;
        private boolean mSendMsg;

        public TaskHandler(LayoutManagerBanner bannerLayoutManager) {
            this.mWeakBanner = new WeakReference(bannerLayoutManager);
        }

        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg != null && this.mSendMsg) {
                int position = msg.what;
                LayoutManagerBanner bannerLayoutManager = (LayoutManagerBanner)this.mWeakBanner.get();
                if (bannerLayoutManager != null) {
                    bannerLayoutManager.getRecyclerView().smoothScrollToPosition(position);
                }
            }

        }

        public void setSendMsg(boolean sendMsg) {
            this.mSendMsg = sendMsg;
        }
    }

    public interface OnSelectedViewListener {
        void onSelectedView(View var1, int var2);
    }
}
