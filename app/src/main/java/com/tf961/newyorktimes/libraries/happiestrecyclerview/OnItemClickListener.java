//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View var1, int var2);

    void onItemLongClick(View var1, int var2);
}
