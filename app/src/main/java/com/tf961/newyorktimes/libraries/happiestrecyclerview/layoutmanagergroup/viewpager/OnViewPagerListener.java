//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview.layoutmanagergroup.viewpager;

public interface OnViewPagerListener {
    void onInitComplete();

    void onPageRelease(boolean var1, int var2);

    void onPageSelected(int var1, boolean var2);
}
