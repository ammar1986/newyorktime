//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.widget.FrameLayout;


import androidx.recyclerview.widget.RecyclerView.LayoutManager;
import androidx.recyclerview.widget.RecyclerView.OnScrollListener;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener;
import com.tf961.newyorktimes.R.styleable;

public class RecyclerView<T extends RecyclerViewAdapter> extends FrameLayout {
    androidx.recyclerview.widget.RecyclerView recyclerView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    SwipeListener swipeListener;
    EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener = null;
    protected boolean isRefreshEnabled = false;
    protected boolean isLoadMore = false;

    public void scrollToPosition(int position) {
        this.recyclerView.scrollToPosition(position);
    }

    public void setNestedScrollingEnabled(boolean enabled) {
        if(this.recyclerView == null)
            this.recyclerView = new androidx.recyclerview.widget.RecyclerView(getContext());
        this.recyclerView.setNestedScrollingEnabled(enabled);
    }

    public LayoutManager getLayoutManager() {
        return this.recyclerView.getLayoutManager();
    }

    public void addOnScrollListener(OnScrollListener listener) {
        this.recyclerView.addOnScrollListener(listener);
    }

    public RecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.obtainStyledAttributes(attrs, styleable.MyRecyclerView, 0, 0);

        try {
            this.isRefreshEnabled = ta.getBoolean(styleable.MyRecyclerView_isRefresh, false);
            this.isLoadMore = ta.getBoolean(styleable.MyRecyclerView_isLoadMore, false);
        } finally {
            ta.recycle();
        }



        if (this.isRefreshEnabled) {
            this.initSwipeRefreshLayout(context);
        } else {
            this.init(context);
        }

    }

    public boolean getIsLoadMore() {
        return this.isLoadMore;
    }

    public void addEndlessRecyclerOnScrollListener(final Context context) {
        if (this.endlessRecyclerOnScrollListener != null) {
            if (this.getRecyclerView() != null) {
                this.getRecyclerView().removeOnScrollListener(this.endlessRecyclerOnScrollListener);
            }

            this.endlessRecyclerOnScrollListener = null;
        }

        this.endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(this.getRecyclerView().getLayoutManager()) {
            public void onLoadMore(int current_page) {
                if (RecyclerView.this.swipeListener != null) {
                    RecyclerView<T>.ConnectionDetector cd = RecyclerView.this.new ConnectionDetector(context);
                    if (cd.isConnectingToInternet()) {
                        RecyclerView.this.swipeListener.loadMore(current_page);
                    } else {
                        RecyclerView.this.swipeListener.onSwipeConnectionError();
                    }
                }

            }
        };
        if (this.endlessRecyclerOnScrollListener != null && this.getRecyclerView() != null) {
            this.getRecyclerView().addOnScrollListener(this.endlessRecyclerOnScrollListener);
        }

    }

    public void setSwipeListener(SwipeListener swipeListener) {
        this.swipeListener = swipeListener;
    }

    void initSwipeRefreshLayout(final Context context) {
        if(this.recyclerView == null)
        this.recyclerView = new androidx.recyclerview.widget.RecyclerView(context);
        this.mSwipeRefreshLayout = new SwipeRefreshLayout(this.recyclerView.getContext());
        this.addView(this.mSwipeRefreshLayout);
        this.recyclerView.setLayoutParams(new LayoutParams(new LayoutParams(-1, -1)));

        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.addView(this.recyclerView);
        this.mSwipeRefreshLayout.addView(frameLayout);
        this.mSwipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            public void onRefresh() {
                RecyclerView<T>.ConnectionDetector cd = RecyclerView.this.new ConnectionDetector(context);
                if (cd.isConnectingToInternet()) {
                    if (RecyclerView.this.swipeListener != null) {
                        RecyclerView.this.swipeListener.onSwipe();
                    }
                } else {
                    RecyclerView.this.mSwipeRefreshLayout.setRefreshing(false);
                    if (RecyclerView.this.swipeListener != null) {
                        RecyclerView.this.swipeListener.onSwipeConnectionError();
                    }
                }

            }
        });
    }

    public void setIsRefreshing(boolean isRefreshing) {
        if (this.mSwipeRefreshLayout != null) {
            this.mSwipeRefreshLayout.setRefreshing(isRefreshing);
        }

    }

    public void onConnectionError() {
        if (this.endlessRecyclerOnScrollListener != null) {
            this.endlessRecyclerOnScrollListener.onLoadMore(this.endlessRecyclerOnScrollListener.current_page);
        }

    }

    private void init(Context context) {
        if(this.recyclerView == null)
        this.recyclerView = new androidx.recyclerview.widget.RecyclerView(context);
        if (VERSION.SDK_INT >= 19) {
            this.recyclerView.setLayoutParams(new LayoutParams(new LayoutParams(-1, -1)));
        }

        this.addView(this.recyclerView);
    }

    public androidx.recyclerview.widget.RecyclerView getRecyclerView() {
        return this.recyclerView;
    }

    public T getAdapter() {
        return (T) this.recyclerView.getAdapter();
    }

    public void setAdapter(T adapter) {
        adapter.isLoadMore = isLoadMore;
        adapter.continu(this);
        this.recyclerView.setAdapter(adapter);
    }

    public void addDivider(Drawable divider, boolean includeAds, boolean includeSections) {
        this.recyclerView.addItemDecoration(((RecyclerViewAdapter)this.getAdapter()).getCustomDividerItemDecoration(divider, includeAds, includeSections));
    }

    private class ConnectionDetector {
        private Context _context;

        public ConnectionDetector(Context context) {
            this._context = context;
        }

        public boolean isConnectingToInternet() {
            @SuppressLint("WrongConstant") ConnectivityManager connectivity = (ConnectivityManager)this._context.getSystemService("connectivity");
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for(int i = 0; i < info.length; ++i) {
                        if (info[i].getState() == State.CONNECTED) {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}
