package com.tf961.newyorktimes.libraries.happiestrecyclerview;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

interface ViewRetriever {
    RecyclerView.ViewHolder getViewHolderForPosition(int var1);

    public static final class RecyclerViewRetriever implements ViewRetriever {
        private final RecyclerView recyclerView;
        private androidx.recyclerview.widget.RecyclerView.ViewHolder currentViewHolder;
        private int currentViewType;

        RecyclerViewRetriever(RecyclerView recyclerView) {
            this.recyclerView = recyclerView;
            this.currentViewType = -1;
        }

        public androidx.recyclerview.widget.RecyclerView.ViewHolder getViewHolderForPosition(int position) {
            if (this.currentViewType != this.recyclerView.getAdapter().getItemViewType(position)) {
                this.currentViewType = this.recyclerView.getAdapter().getItemViewType(position);
                this.currentViewHolder = this.recyclerView.getAdapter().createViewHolder((ViewGroup)this.recyclerView.getParent(), this.currentViewType);
            }

            return this.currentViewHolder;
        }
    }
}
