package com.tf961.newyorktimes.data;

import com.androidnetworking.error.ANError;

public  class ExceptionStatusCode{
         public static Boolean isHttp401Error(Throwable throwable){
             return ((ANError)throwable).getErrorCode() == 401;
         }
    public static Boolean isHttp410Error(Throwable throwable){
        return ((ANError)throwable).getErrorCode() == 410;
    }
    }