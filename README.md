NewYorkTime is an application that shows 
the most popular articles viewed for a past specific period.

This project consists of 5 packages as listed below

1. "base"      : contains an abstract classes (base classes for activities, adapters, viewholder, ect....) 
2. "data"      : contains the data layer including network layer and database
3. "libraries" : contains a Custom built recyclerview helper library written by me and I use it in all my project.
4. "Utils"     : contains some helper classes
5. "ui"        : this package is considered as our workspace where all pages will be created,
                 For now there is only one package inside contains the page of articles.
 
Design Pattern: 
MVVM is the pattern used in this project with databinding and RXjava

Testing:
you can find the MainActivityList class for testing and insure that there 
is at least one eleme                                                                     k l.l ,nt displayed.(right click on class file and click run)

                
*Installing the app on device* : go to root file and open the commond promp and run gradlew installDebug
*building an APK* : go to root file and open the commond promp and run gradlew assembleDebug
