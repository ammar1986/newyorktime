package com.tf961.newyorktimes;

import android.view.View;
import android.view.ViewGroup;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.tf961.newyorktimes.libraries.happiestrecyclerview.RecyclerView;
import com.tf961.newyorktimes.ui.mainPage.MainActivity;
import com.tf961.newyorktimes.ui.mainPage.list.MainRecylerView;

import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;
@RunWith(AndroidJUnit4.class)
public class MainActivityList {

    @Rule
    public final ActivityTestRule<MainActivity> mMainActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testSample(){
        Espresso.registerIdlingResources(mMainActivityTestRule.getActivity().idlingResource);

        onView(nthChildOf(withId(R.id.recyclerView), 0))
                .perform(
                        RecyclerViewActions.actionOnItemAtPosition(0, click())
                );
        onView(withText("test message")).inRoot(withDecorView(not(is(mMainActivityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }

    public static Matcher<View> nthChildOf(final Matcher<View> parentMatcher, final int childPosition) {
        return new TypeSafeMatcher<View>() {
            /**
             * Generates a description of the object.  The description may be part of a
             * a description of a larger object of which this is just a component, so it
             * should be worded appropriately.
             *
             * @param description The description to be built or appended to.
             */
            @Override
            public void describeTo(org.hamcrest.Description description) {
                description.appendText("with "+childPosition+" child view of type parentMatcher");
            }


            @Override
            public boolean matchesSafely(View view) {
                if (!(view.getParent() instanceof ViewGroup)) {
                    return parentMatcher.matches(view.getParent());
                }

                ViewGroup group = (ViewGroup) view.getParent();
                return parentMatcher.matches(view.getParent()) && group.getChildAt(childPosition).equals(view);
            }
        };
    }

}
