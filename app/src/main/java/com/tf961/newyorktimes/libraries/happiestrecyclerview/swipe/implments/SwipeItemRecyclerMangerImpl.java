//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.implments;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.SwipeLayout;

public class SwipeItemRecyclerMangerImpl extends SwipeItemMangerImpl {
    protected Adapter mAdapter;

    public SwipeItemRecyclerMangerImpl(Adapter adapter) {
        super(adapter);
        this.mAdapter = adapter;
    }

    public void bindView(View target, int position) {
        int resId = getSwipeLayoutId(position);

        OnLayoutListener onLayoutListener = new OnLayoutListener(position);
        SwipeLayout swipeLayout = (SwipeLayout) target.findViewById(resId);
        if (swipeLayout == null)
            throw new IllegalStateException("can not find SwipeLayout in target view");

        if (swipeLayout.getTag(resId) == null) {
            SwipeMemory swipeMemory = new SwipeMemory(position);
            swipeLayout.addSwipeListener(swipeMemory);
            swipeLayout.addOnLayoutListener(onLayoutListener);
            swipeLayout.setTag(resId, new ValueBox(position, swipeMemory, onLayoutListener));
            mShownLayouts.add(swipeLayout);
        } else {
            ValueBox valueBox = (ValueBox) swipeLayout.getTag(resId);
            valueBox.swipeMemory.setPosition(position);
            valueBox.onLayoutListener.setPosition(position);
            valueBox.position = position;
        }
    }

    public void initialize(View target, int position) {
    }

    public void updateConvertView(View target, int position) {
    }
}
