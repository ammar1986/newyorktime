/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.tf961.newyorktimes.data;

import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.tf961.newyorktimes.data.local.prefs.PreferencesHelper;
import com.tf961.newyorktimes.data.model.api.BaseResponse;
import com.tf961.newyorktimes.data.model.api.mainPage.NYTimeMockObject;
import com.tf961.newyorktimes.data.remote.ApiHeader;
import com.tf961.newyorktimes.data.remote.ApiHelper;

import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Function;

 /*
 Created by assaad on 07/07/17.
 */
public class AppDataManager implements DataManager {

    private final ApiHelper mApiHelper;


    private final PreferencesHelper mPreferencesHelper;

    private final Gson mGson;

    private static AppDataManager appDataManager;
    public static AppDataManager getInstance(Context context, PreferencesHelper preferencesHelper, ApiHelper apiHelper, Gson gson){
        if(appDataManager == null)
            appDataManager = new  AppDataManager( context,  preferencesHelper,  apiHelper,  gson);
        return appDataManager;
    }
    public AppDataManager(Context context, PreferencesHelper preferencesHelper, ApiHelper apiHelper, Gson gson) {
        Context mContext = context;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
        mGson = gson;
    }


    private <T> Single<T> authenticatedSingleApi(Single<T> single) {
        return single.onErrorResumeNext(refreshTokenAndRetryObser(single));
    }


    private <T> Function<Throwable, ? extends Single<? extends T>> refreshTokenAndRetryObser(final Single<T> toBeResumed) {
        return (Function<Throwable, Single<? extends T>>) throwable -> {
//            if (ExceptionStatusCode.isHttp401Error(throwable)) {
//                mPreferencesHelper.setRefreshingToken(true);
//                return refreshToken().flatMap(refreshTokenResponse -> {
//                    mPreferencesHelper.setRefreshingToken(false);
//                    setAccessToken(refreshTokenResponse.getData().getToken());
//                    return toBeResumed;
//                });
//            } else if (ExceptionStatusCode.isHttp410Error(throwable)) {
//                Intent intent = new Intent();
//                intent.setAction(AppConstants.ACTION_UPDATE_AVAILABLE);
//                ApplicationContext.getInstance().sendBroadcast(intent);
//                return Single.error(throwable);
//            } else
            return Single.error(throwable);
        };
    }


     @Override
     public ApiHeader getApiHeader() {
         return mApiHelper.getApiHeader();
     }

     @Override
     public Single<BaseResponse<ArrayList<NYTimeMockObject>>> getArticles(int period) {
         return mApiHelper.getArticles(period);
     }
 }
