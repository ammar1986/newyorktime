package com.tf961.newyorktimes.ui.mainPage.list;

import android.content.Context;
import android.util.AttributeSet;

import com.tf961.newyorktimes.libraries.happiestrecyclerview.RecyclerView;

public class MainRecylerView extends RecyclerView<MainPageAdapter> {

    public MainRecylerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
