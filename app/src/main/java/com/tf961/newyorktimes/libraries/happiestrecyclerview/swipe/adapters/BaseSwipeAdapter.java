//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.SwipeLayout;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.implments.SwipeItemAdapterMangerImpl;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.interfaces.SwipeAdapterInterface;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.interfaces.SwipeItemMangerInterface;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.util.Attributes.Mode;

public abstract class BaseSwipeAdapter extends BaseAdapter implements SwipeItemMangerInterface, SwipeAdapterInterface {
    protected SwipeItemAdapterMangerImpl mItemManger = new SwipeItemAdapterMangerImpl(this);

    public BaseSwipeAdapter() {
    }

    public abstract int getSwipeLayoutResourceId(int var1);

    public abstract View generateView(int var1, ViewGroup var2);

    public abstract void fillValues(int var1, View var2);

    public final View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (convertView == null) {
            v = this.generateView(position, parent);
            this.mItemManger.initialize(v, position);
        } else {
            this.mItemManger.updateConvertView(convertView, position);
        }

        this.fillValues(position, v);
        return v;
    }

    public void openItem(int position) {
        this.mItemManger.openItem(position);
    }

    public void closeItem(int position) {
        this.mItemManger.closeItem(position);
    }

    public void closeAllExcept(SwipeLayout layout) {
        this.mItemManger.closeAllExcept(layout);
    }

    public void closeAllItems() {
        this.mItemManger.closeAllItems();
    }

    public List<Integer> getOpenItems() {
        return this.mItemManger.getOpenItems();
    }

    public List<SwipeLayout> getOpenLayouts() {
        return this.mItemManger.getOpenLayouts();
    }

    public void removeShownLayouts(SwipeLayout layout) {
        this.mItemManger.removeShownLayouts(layout);
    }

    public boolean isOpen(int position) {
        return this.mItemManger.isOpen(position);
    }

    public Mode getMode() {
        return this.mItemManger.getMode();
    }

    public void setMode(Mode mode) {
        this.mItemManger.setMode(mode);
    }
}
