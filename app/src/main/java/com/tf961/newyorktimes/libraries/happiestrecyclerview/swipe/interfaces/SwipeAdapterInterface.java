//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.interfaces;

public interface SwipeAdapterInterface {
    int getSwipeLayoutResourceId(int var1);
}
