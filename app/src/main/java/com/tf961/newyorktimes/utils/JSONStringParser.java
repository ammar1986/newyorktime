package com.tf961.newyorktimes.utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONStringParser {
    public static JSONObject isJSONObject(String jsonString) {
        try {
            return new JSONObject(jsonString);
        }catch (Exception e) {
            return null;
        }
    }
    public static JSONArray isJSONArray(String jsonString) {
        try {
            return new JSONArray(jsonString);
        }catch (Exception e) {
            return null;
        }
    }
}
