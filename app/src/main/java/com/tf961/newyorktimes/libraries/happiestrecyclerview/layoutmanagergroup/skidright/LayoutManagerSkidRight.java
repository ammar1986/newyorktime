//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview.layoutmanagergroup.skidright;

import android.view.View;
import android.view.View.MeasureSpec;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.LayoutManager;
import androidx.recyclerview.widget.RecyclerView.LayoutParams;
import androidx.recyclerview.widget.RecyclerView.Recycler;
import androidx.recyclerview.widget.RecyclerView.State;

import java.util.ArrayList;

import com.tf961.newyorktimes.libraries.happiestrecyclerview.layoutmanagergroup.echelon.ItemViewInfo;

public class LayoutManagerSkidRight extends LayoutManager {
    private boolean mHasChild = false;
    private int mItemViewWidth;
    private int mItemViewHeight;
    private int mScrollOffset = 2147483647;
    private float mItemHeightWidthRatio;
    private float mScale;
    private int mItemCount;
    private SkidRightSnapHelper mSkidRightSnapHelper;

    public LayoutManagerSkidRight(float itemHeightWidthRatio, float scale) {
        this.mItemHeightWidthRatio = itemHeightWidthRatio;
        this.mScale = scale;
        this.mSkidRightSnapHelper = new SkidRightSnapHelper();
    }

    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2);
    }

    public void onAttachedToWindow(RecyclerView view) {
        super.onAttachedToWindow(view);
        this.mSkidRightSnapHelper.attachToRecyclerView(view);
    }

    public int getFixedScrollPosition(int direction, float fixValue) {
        if (this.mHasChild) {
            if (this.mScrollOffset % this.mItemViewWidth == 0) {
                return -1;
            } else {
                float position = (float)this.mScrollOffset * 1.0F / (float)this.mItemViewWidth;
                return this.convert2AdapterPosition((int)(direction > 0 ? position + fixValue : position + (1.0F - fixValue)) - 1);
            }
        } else {
            return -1;
        }
    }

    public void onLayoutChildren(Recycler recycler, State state) {
        if (state.getItemCount() != 0 && !state.isPreLayout()) {
            this.removeAndRecycleAllViews(recycler);
            if (!this.mHasChild) {
                this.mItemViewHeight = this.getVerticalSpace();
                this.mItemViewWidth = (int)((float)this.mItemViewHeight / this.mItemHeightWidthRatio);
                this.mHasChild = true;
            }

            this.mItemCount = this.getItemCount();
            this.mScrollOffset = this.makeScrollOffsetWithinRange(this.mScrollOffset);
            this.fill(recycler);
        }
    }

    public void fill(Recycler recycler) {
        int bottomItemPosition = (int)Math.floor((double)(this.mScrollOffset / this.mItemViewWidth));
        int bottomItemVisibleSize = this.mScrollOffset % this.mItemViewWidth;
        float offsetPercent = (float)bottomItemVisibleSize * 1.0F / (float)this.mItemViewWidth;
        int space = this.getHorizontalSpace();
        ArrayList<ItemViewInfo> layoutInfos = new ArrayList();
        int layoutCount = bottomItemPosition - 1;
        int startPos = 1;

        int endPos;
        for(endPos = space - this.mItemViewWidth; layoutCount >= 0; ++startPos) {
            double maxOffset = (double)((this.getHorizontalSpace() - this.mItemViewWidth) / 2) * Math.pow((double)this.mScale, (double)startPos);
            int start = (int)((double)endPos - (double)offsetPercent * maxOffset);
            ItemViewInfo info = new ItemViewInfo(start, (float)(Math.pow((double)this.mScale, (double)(startPos - 1)) * (double)(1.0F - offsetPercent * (1.0F - this.mScale))), offsetPercent, (float)start * 1.0F / (float)space);
            layoutInfos.add(0, info);
            endPos = (int)((double)endPos - maxOffset);
            if (endPos <= 0) {
                info.setTop((int)((double)endPos + maxOffset));
                info.setPositionOffset(0.0F);
                info.setLayoutPercent((float)(info.getTop() / space));
                info.setScaleXY((float)Math.pow((double)this.mScale, (double)(startPos - 1)));
                break;
            }

            --layoutCount;
        }

        if (bottomItemPosition < this.mItemCount) {
            layoutCount = space - bottomItemVisibleSize;
            layoutInfos.add((new ItemViewInfo(layoutCount, 1.0F, (float)bottomItemVisibleSize * 1.0F / (float)this.mItemViewWidth, (float)layoutCount * 1.0F / (float)space)).setIsBottom());
        } else {
            --bottomItemPosition;
        }

        layoutCount = layoutInfos.size();
        startPos = bottomItemPosition - (layoutCount - 1);
        endPos = bottomItemPosition;
        int childCount = this.getChildCount();

        int i;
        for(i = childCount - 1; i >= 0; --i) {
            View childView = this.getChildAt(i);
            int pos = this.convert2LayoutPosition(this.getPosition(childView));
            if (pos > endPos || pos < startPos) {
                this.removeAndRecycleView(childView, recycler);
            }
        }

        this.detachAndScrapAttachedViews(recycler);

        for(i = 0; i < layoutCount; ++i) {
            this.fillChild(recycler.getViewForPosition(this.convert2AdapterPosition(startPos + i)), (ItemViewInfo)layoutInfos.get(i));
        }

    }

    private void fillChild(View view, ItemViewInfo layoutInfo) {
        this.addView(view);
        this.measureChildWithExactlySize(view);
        int scaleFix = (int)((float)this.mItemViewWidth * (1.0F - layoutInfo.getScaleXY()) / 2.0F);
        int top = this.getPaddingTop();
        this.layoutDecoratedWithMargins(view, layoutInfo.getTop() - scaleFix, top, layoutInfo.getTop() + this.mItemViewWidth - scaleFix, top + this.mItemViewHeight);
        ViewCompat.setScaleX(view, layoutInfo.getScaleXY());
        ViewCompat.setScaleY(view, layoutInfo.getScaleXY());
    }

    private void measureChildWithExactlySize(View child) {
        LayoutParams lp = (LayoutParams)child.getLayoutParams();
        int widthSpec = MeasureSpec.makeMeasureSpec(this.mItemViewWidth - lp.leftMargin - lp.rightMargin, 1073741824);
        int heightSpec = MeasureSpec.makeMeasureSpec(this.mItemViewHeight - lp.topMargin - lp.bottomMargin, 1073741824);
        child.measure(widthSpec, heightSpec);
    }

    private int makeScrollOffsetWithinRange(int scrollOffset) {
        return Math.min(Math.max(this.mItemViewWidth, scrollOffset), this.mItemCount * this.mItemViewWidth);
    }

    public int scrollHorizontallyBy(int dx, Recycler recycler, State state) {
        int pendingScrollOffset = this.mScrollOffset + dx;
        this.mScrollOffset = this.makeScrollOffsetWithinRange(pendingScrollOffset);
        this.fill(recycler);
        return this.mScrollOffset - pendingScrollOffset + dx;
    }

    public int calculateDistanceToPosition(int targetPos) {
        int pendingScrollOffset = this.mItemViewWidth * (this.convert2LayoutPosition(targetPos) + 1);
        return pendingScrollOffset - this.mScrollOffset;
    }

    public void scrollToPosition(int position) {
        if (position > 0 && position < this.mItemCount) {
            this.mScrollOffset = this.mItemViewWidth * (this.convert2LayoutPosition(position) + 1);
            this.requestLayout();
        }

    }

    public boolean canScrollHorizontally() {
        return true;
    }

    public int convert2AdapterPosition(int layoutPosition) {
        return this.mItemCount - 1 - layoutPosition;
    }

    public int convert2LayoutPosition(int adapterPostion) {
        return this.mItemCount - 1 - adapterPostion;
    }

    public int getVerticalSpace() {
        return this.getHeight() - this.getPaddingTop() - this.getPaddingBottom();
    }

    public int getHorizontalSpace() {
        return this.getWidth() - this.getPaddingLeft() - this.getPaddingRight();
    }
}
