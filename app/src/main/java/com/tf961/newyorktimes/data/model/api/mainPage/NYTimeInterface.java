package com.tf961.newyorktimes.data.model.api.mainPage;

public interface NYTimeInterface   {
    String getNYDate();
    String getNYTitle();
    String getNYImage();
    String getNYDescription();
}
