//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

import android.view.View;

interface StickyHeaderListener {
    void headerAttached(View var1, int var2);

    void headerDetached(View var1, int var2);
}
