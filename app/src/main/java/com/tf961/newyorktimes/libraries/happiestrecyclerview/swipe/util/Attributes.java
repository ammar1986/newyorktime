//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.util;

public class Attributes {
    public Attributes() {
    }

    public static enum Mode {
        Single,
        Multiple;

        private Mode() {
        }
    }
}
