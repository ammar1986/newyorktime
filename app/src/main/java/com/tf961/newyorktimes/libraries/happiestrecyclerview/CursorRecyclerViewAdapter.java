//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.util.SparseArray;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.SwipeLayout;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.implments.SwipeItemRecyclerMangerImpl;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.interfaces.SwipeAdapterInterface;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.interfaces.SwipeItemMangerInterface;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.util.Attributes;

import java.util.ArrayList;
import java.util.List;


public abstract class CursorRecyclerViewAdapter<VH extends ViewHolder> extends Adapter<VH> implements StickyHeaderHandler, SwipeAdapterInterface, SwipeItemMangerInterface {
    public static final int TYPE_HEADER = 0;
    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;
    private SparseArray<String> mSectionsIndexer;
    public Activity mContext;
    private Cursor mCursor;
    private boolean mDataValid;
    private int mRowIdColumn;
    private DataSetObserver mDataSetObserver;
    int SelectedBackroundColorID;
    int menuID;
    MenuRecyclerListener menuRecyclerListener;
    RecyclerView recyclerView;
    boolean isMultiSelect = false;
    ActionMode mActionMode;
    private ArrayList multiselect_list = new ArrayList();
    public HeaderHolder lastHeader;
    SwipeItemRecyclerMangerImpl mItemManger;
    private Callback mActionModeCallback = new Callback() {
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.clear();
            MenuInflater inflater = mode.getMenuInflater();
            if (CursorRecyclerViewAdapter.this.menuID != 0) {
                inflater.inflate(CursorRecyclerViewAdapter.this.menuID, menu);
            }

            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            if (CursorRecyclerViewAdapter.this.menuRecyclerListener != null) {
                CursorRecyclerViewAdapter.this.menuRecyclerListener.onMenuItemSelected(item.getItemId());
            }

            if (mode != null) {
                mode.finish();
            }

            return false;
        }

        public void onDestroyActionMode(ActionMode mode) {
            CursorRecyclerViewAdapter.this.mActionMode = null;
            CursorRecyclerViewAdapter.this.multiselect_list = new ArrayList();
            CursorRecyclerViewAdapter.this.notifyDataSetChanged();
            (new Handler()).postDelayed(new Runnable() {
                public void run() {
                    CursorRecyclerViewAdapter.this.isMultiSelect = false;
                }
            }, 200L);
        }
    };

    public CursorRecyclerViewAdapter(Activity context, Cursor cursor, RecyclerView recyclerView) {
        this.mContext = context;
        this.mCursor = cursor;
        this.mDataValid = cursor != null;
        this.recyclerView = recyclerView;
        this.mRowIdColumn = this.mDataValid ? this.mCursor.getColumnIndex("_id") : -1;
        this.mDataSetObserver = new CursorRecyclerViewAdapter.NotifyingDataSetObserver();
        if (this.mCursor != null) {
            this.mCursor.registerDataSetObserver(this.mDataSetObserver);
        }

        this.mSectionsIndexer = new SparseArray();
        if (this.isStickyHeader()) {
            StickyLayoutManager stickyLayoutManager = new StickyLayoutManager(context, this.getOrientation(), false, this);
            stickyLayoutManager.elevateHeaders(true);
            recyclerView.getRecyclerView().setLayoutManager(stickyLayoutManager);
        } else {
            LinearLayoutManager linearLayoutmanager = new LinearLayoutManager(context, this.getOrientation(), false);
            recyclerView.getRecyclerView().setLayoutManager(linearLayoutmanager);
        }

        if (recyclerView.getIsLoadMore()) {
            recyclerView.addEndlessRecyclerOnScrollListener(context);
        }

        this.mItemManger = new SwipeItemRecyclerMangerImpl(this);
    }

    public void setOnItemClickedListener(Context context, int SelectedBackroundColorID, final OnItemClickListener onItemClickListener) {
        this.SelectedBackroundColorID = SelectedBackroundColorID;
        this.recyclerView.getRecyclerView().addOnItemTouchListener(new RecyclerItemClickListener(context, this.recyclerView.getRecyclerView(), new com.tf961.newyorktimes.libraries.happiestrecyclerview.RecyclerItemClickListener.OnItemClickListener() {
            public void onItemClick(View view, int position) {
                if (!CursorRecyclerViewAdapter.this.isMultiSelect) {
                    onItemClickListener.onItemClick(view, position);
                }

            }

            public void onItemLongClick(View view, int position) {
                if (!CursorRecyclerViewAdapter.this.isMultiSelect) {
                    onItemClickListener.onItemLongClick(view, position);
                }

            }
        }));
    }

    public void enableMultiSelection(Context context, int SelectedBackroundColorID, MenuRecyclerListener menuRecyclerListener, int menuID) {
        this.menuID = menuID;
        this.SelectedBackroundColorID = SelectedBackroundColorID;
        this.menuRecyclerListener = menuRecyclerListener;
        this.recyclerView.getRecyclerView().addOnItemTouchListener(new RecyclerItemClickListener(context, this.recyclerView.getRecyclerView(), new com.tf961.newyorktimes.libraries.happiestrecyclerview.RecyclerItemClickListener.OnItemClickListener() {
            public void onItemClick(View view, int position) {
                if (CursorRecyclerViewAdapter.this.mSectionsIndexer.indexOfKey(position) < 0) {
                    if (CursorRecyclerViewAdapter.this.isMultiSelect) {
                        CursorRecyclerViewAdapter.this.multi_select(position - CursorRecyclerViewAdapter.this.countNumberSectionsBefore(position) + "");
                    }

                }
            }

            public void onItemLongClick(View view, int position) {
                if (CursorRecyclerViewAdapter.this.mSectionsIndexer.indexOfKey(position) < 0) {
                    if (!CursorRecyclerViewAdapter.this.isMultiSelect) {
                        CursorRecyclerViewAdapter.this.multiselect_list = new ArrayList();
                        CursorRecyclerViewAdapter.this.isMultiSelect = true;
                        if (CursorRecyclerViewAdapter.this.mActionMode == null) {
                            CursorRecyclerViewAdapter.this.mActionMode = CursorRecyclerViewAdapter.this.recyclerView.startActionMode(CursorRecyclerViewAdapter.this.mActionModeCallback);
                        }
                    }

                    CursorRecyclerViewAdapter.this.multi_select(position - CursorRecyclerViewAdapter.this.countNumberSectionsBefore(position) + "");
                }
            }
        }));
    }

    void multi_select(String position) {
        if (this.mActionMode != null) {
            if (this.multiselect_list.contains(position)) {
                this.multiselect_list.remove(position);
            } else {
                this.multiselect_list.add(position);
            }

            if (this.multiselect_list.size() > 0) {
                this.mActionMode.setTitle("" + this.multiselect_list.size());
            } else {
                this.mActionMode.setTitle("");
                this.mActionMode.finish();
                this.mActionMode = null;
                this.multiselect_list = new ArrayList();
                this.notifyDataSetChanged();
            }

            this.notifyDataSetChanged();
        }

    }

    public Cursor getCursor() {
        return this.mCursor;
    }

    public <T> ArrayList<T> getMultiSelectedList() {
        return this.multiselect_list;
    }

    protected abstract ViewHolder getHeaderHolders(View var1);

    public int getItemCount() {
        if (this.mDataValid && this.mCursor != null) {
            return this.getCursor() == null ? 0 : this.getCursor().getCount() + this.mSectionsIndexer.size();
        } else {
            return 0;
        }
    }

    public abstract boolean attachAlwaysLastHeader();

    public abstract int getOrientation();

    public abstract String getSectionCondition(Cursor var1);

    public abstract boolean isSection();

    public abstract boolean isStickyHeader();

    public abstract int getItemType(int var1);

    public abstract int getHeaderLayout();

    private void calculateSectionHeaders() {
        this.mSectionsIndexer.clear();
        int i = 0;
        String previous = "";
        int count = 0;
        Cursor c = this.getCursor();
        if (c != null && !c.isClosed()) {
            c.moveToFirst();

            do {
                String temp = this.getSectionCondition(c);
                if (temp == null || !previous.equals(temp)) {
                    this.mSectionsIndexer.put(i + count, temp);
                    previous = temp;
                    ++count;
                }

                ++i;
            } while(c.moveToNext());

        }
    }

    private int countNumberSectionsBefore(int position) {
        int count = 0;

        for(int i = 0; i < this.mSectionsIndexer.size(); ++i) {
            if (position > this.mSectionsIndexer.keyAt(i)) {
                ++count;
            }
        }

        return count;
    }

    public long getItemId(int position) {
        return this.mDataValid && this.mCursor != null && this.mCursor.moveToPosition(position) ? this.mCursor.getLong(this.mRowIdColumn) : 0L;
    }

    public SparseArray<String> getAdapterData() {
        return this.mSectionsIndexer.size() == 0 ? null : this.mSectionsIndexer;
    }

    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(true);
    }

    public abstract void onBindViewHolder(VH var1, Cursor var2, int var3);

    public void onBindViewHolder(VH viewHolder, int position) {
        if (!this.mDataValid) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        } else {
            if (!this.isSection()) {
                viewHolder.setItemPosition(position);
                this.onBindViewHolder(viewHolder, this.mCursor, position);
                if (this.getMultiSelectedList().contains(position + "")) {
                    viewHolder.itemView.setBackgroundColor(this.SelectedBackroundColorID);
                } else {
                    viewHolder.itemView.setBackgroundColor(this.mContext.getResources().getColor(17170445));
                }
            } else if (this.getItemViewType(position) == 0) {
                this.createHeaderViewHolder(viewHolder, position - this.countNumberSectionsBefore(position));
            } else {
                viewHolder.setItemPosition(position - this.countNumberSectionsBefore(position));
                this.onBindViewHolder(viewHolder, this.mCursor, position - this.countNumberSectionsBefore(position));
                if (this.getMultiSelectedList().contains(position - this.countNumberSectionsBefore(position) + "")) {
                    viewHolder.itemView.setBackgroundColor(this.SelectedBackroundColorID);
                } else {
                    viewHolder.itemView.setBackgroundColor(this.mContext.getResources().getColor(17170445));
                }
            }

        }
    }

    public abstract ViewHolder onCreateViewHolders(ViewGroup var1, int var2);

    /** @deprecated */
    @Deprecated
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = null;
        switch(viewType) {
        case 0:
            v = layoutInflater.inflate(this.getHeaderLayout(), parent, false);
            return (VH) this.getHeaderHolders(v);
        default:
            return (VH) this.onCreateViewHolders(parent, viewType);
        }
    }

    public HeaderHolder createHeaderViewHolder(ViewHolder viewHolder, int position) {
        if (position < 0) {
            return null;
        } else {
            this.getCursor().moveToPosition(position);
            String date = this.getSectionCondition(this.getCursor());
            HeaderHolder headerHolder = this.bindHeaderViewHolder(viewHolder, position);
            if (!this.attachAlwaysLastHeader()) {
                if (date.equals(this.getLastHeaderData())) {
                    if (((StickyLayoutManager)this.recyclerView.getRecyclerView().getLayoutManager()).findFirstVisibleItemPosition() > headerHolder.getAdapterPosition()) {
                        headerHolder.setVisibility(false);
                    } else {
                        headerHolder.setVisibility(true);
                    }

                    this.lastHeader = headerHolder;
                } else {
                    headerHolder.setVisibility(true);
                    if (this.lastHeader != null) {
                        this.lastHeader.setVisibility(true);
                    }
                }
            }

            return headerHolder;
        }
    }

    public abstract HeaderHolder bindHeaderViewHolder(ViewHolder var1, int var2);

    public void changeCursor(Cursor cursor) {
        Cursor old = this.swapCursor(cursor);
        if (old != null) {
            old.close();
        }

    }

    public String getLastHeaderData() {
        return (String)this.mSectionsIndexer.get(this.mSectionsIndexer.keyAt(this.mSectionsIndexer.size() - 1));
    }

    public Cursor swapCursor(Cursor newCursor) {
        if (newCursor == this.mCursor) {
            return null;
        } else {
            Cursor oldCursor = this.mCursor;
            if (oldCursor != null && this.mDataSetObserver != null) {
                oldCursor.unregisterDataSetObserver(this.mDataSetObserver);
            }

            this.mCursor = newCursor;
            if (this.mCursor != null) {
                if (this.mDataSetObserver != null) {
                    this.mCursor.registerDataSetObserver(this.mDataSetObserver);
                }

                this.mRowIdColumn = newCursor.getColumnIndexOrThrow("_id");
                this.mDataValid = true;
                this.notifyDataSetChanged();
            } else {
                this.mRowIdColumn = -1;
                this.mDataValid = false;
                this.notifyDataSetChanged();
            }

            if (oldCursor != null) {
                oldCursor.close();
            }

            if (this.isSection()) {
                this.calculateSectionHeaders();
            }

            this.recyclerView.setIsRefreshing(false);
            return oldCursor;
        }
    }

    public int getItemViewType(int position) {
        if (!this.isSection()) {
            return this.getItemType(position);
        } else {
            return this.mSectionsIndexer.indexOfKey(position) >= 0 ? 0 : this.getItemType(position - this.countNumberSectionsBefore(position));
        }
    }

    public int getAdapterPosition(VH viewHolder) {
        int position = viewHolder.getAdapterPosition();
        return position - this.countNumberSectionsBefore(position);
    }

    public void openItem(int position) {
        this.mItemManger.openItem(position);
    }

    public void closeItem(int position) {
        this.mItemManger.closeItem(position);
    }

    public void closeAllExcept(SwipeLayout layout) {
        this.mItemManger.closeAllExcept(layout);
    }

    public void closeAllItems() {
        this.mItemManger.closeAllItems();
    }

    public List<Integer> getOpenItems() {
        return this.mItemManger.getOpenItems();
    }

    public List<SwipeLayout> getOpenLayouts() {
        return this.mItemManger.getOpenLayouts();
    }

    public void removeShownLayouts(SwipeLayout layout) {
        this.mItemManger.removeShownLayouts(layout);
    }

    public boolean isOpen(int position) {
        return this.mItemManger.isOpen(position);
    }

    public Attributes.Mode getMode() {
        return this.mItemManger.getMode();
    }

    public void setMode(Attributes.Mode mode) {
        this.mItemManger.setMode(mode);
    }

    public void onSwipeClicked(SwipeLayout swipeLayout) {
        this.mItemManger.removeShownLayouts(swipeLayout);
        this.mItemManger.closeAllItems();
    }

    private class NotifyingDataSetObserver extends DataSetObserver {
        private NotifyingDataSetObserver() {
        }

        public void onChanged() {
            super.onChanged();
            CursorRecyclerViewAdapter.this.mDataValid = true;
            CursorRecyclerViewAdapter.this.notifyDataSetChanged();
        }

        public void onInvalidated() {
            super.onInvalidated();
            CursorRecyclerViewAdapter.this.mDataValid = false;
            CursorRecyclerViewAdapter.this.notifyDataSetChanged();
        }
    }
}
