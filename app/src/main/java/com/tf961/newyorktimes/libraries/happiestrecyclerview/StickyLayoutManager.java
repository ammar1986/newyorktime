//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

import android.content.Context;
import android.util.SparseArray;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Recycler;
import androidx.recyclerview.widget.RecyclerView.State;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.tf961.newyorktimes.libraries.happiestrecyclerview.ViewRetriever.RecyclerViewRetriever;

class StickyLayoutManager extends LinearLayoutManager {
    private StickyHeaderPositioner positioner;
    private StickyHeaderHandler headerHandler;
    private List<Integer> headerPositions;
    private RecyclerViewRetriever viewRetriever;
    private int headerElevation;
    @Nullable
    private StickyHeaderListener listener;

    public StickyLayoutManager(Context context, StickyHeaderHandler headerHandler) {
        this(context, 1, false, headerHandler);
        this.init(headerHandler);
    }

    public StickyLayoutManager(Context context, int orientation, boolean reverseLayout, StickyHeaderHandler headerHandler) {
        super(context, orientation, reverseLayout);
        this.headerPositions = new ArrayList();
        this.headerElevation = -1;
        this.init(headerHandler);
    }

    private void init(StickyHeaderHandler stickyHeaderHandler) {
        Preconditions.checkNotNull(stickyHeaderHandler, "StickyHeaderHandler == null");
        this.headerHandler = stickyHeaderHandler;
    }

    public void setStickyHeaderListener(@Nullable StickyHeaderListener listener) {
        this.listener = listener;
        if (this.positioner != null) {
            this.positioner.setListener(listener);
        }

    }

    public void elevateHeaders(boolean elevateHeaders) {
        this.headerElevation = elevateHeaders ? 5 : -1;
        this.elevateHeaders(this.headerElevation);
    }

    public void elevateHeaders(int dp) {
        this.headerElevation = dp;
        if (this.positioner != null) {
            this.positioner.setElevateHeaders(dp);
        }

    }

    public void onLayoutChildren(Recycler recycler, State state) {
        super.onLayoutChildren(recycler, state);
        this.cacheHeaderPositions();
        if (this.positioner != null) {
            this.runPositionerInit();
        }

    }

    public int scrollVerticallyBy(int dy, Recycler recycler, State state) {
        int scroll = super.scrollVerticallyBy(dy, recycler, state);
        if (Math.abs(scroll) > 0 && this.positioner != null) {
            this.positioner.updateHeaderState(this.findFirstVisibleItemPosition(), this.getVisibleHeaders(), this.viewRetriever, this.findFirstCompletelyVisibleItemPosition() == 0);
        }

        return scroll;
    }

    public int scrollHorizontallyBy(int dx, Recycler recycler, State state) {
        int scroll = super.scrollHorizontallyBy(dx, recycler, state);
        if (Math.abs(scroll) > 0 && this.positioner != null) {
            this.positioner.updateHeaderState(this.findFirstVisibleItemPosition(), this.getVisibleHeaders(), this.viewRetriever, this.findFirstCompletelyVisibleItemPosition() == 0);
        }

        return scroll;
    }

    public void removeAndRecycleAllViews(Recycler recycler) {
        super.removeAndRecycleAllViews(recycler);
        if (this.positioner != null) {
            this.positioner.clearHeader();
        }

    }

    public void onAttachedToWindow(RecyclerView view) {
        Preconditions.validateParentView(view);
        this.viewRetriever = new RecyclerViewRetriever(view);
        this.positioner = new StickyHeaderPositioner(view);
        this.positioner.setElevateHeaders(this.headerElevation);
        this.positioner.setListener(this.listener);
        if (this.headerPositions.size() > 0) {
            this.positioner.setHeaderPositions(this.headerPositions);
            this.runPositionerInit();
        }

        super.onAttachedToWindow(view);
    }

    private void runPositionerInit() {
        this.positioner.reset(this.getOrientation());
        this.positioner.updateHeaderState(this.findFirstVisibleItemPosition(), this.getVisibleHeaders(), this.viewRetriever, this.findFirstCompletelyVisibleItemPosition() == 0);
    }

    private Map<Integer, View> getVisibleHeaders() {
        Map<Integer, View> visibleHeaders = new LinkedHashMap();

        for(int i = 0; i < this.getChildCount(); ++i) {
            View view = this.getChildAt(i);
            int dataPosition = this.getPosition(view);
            if (this.headerPositions.contains(dataPosition)) {
                visibleHeaders.put(dataPosition, view);
            }
        }

        return visibleHeaders;
    }

    private int cacheHeaderPositions() {
        this.headerPositions.clear();
        SparseArray<String> count = this.headerHandler.getAdapterData();
        if (count == null) {
            if (this.positioner != null) {
                this.positioner.setHeaderPositions(this.headerPositions);
            }

            return 0;
        } else {
            for(int i = 0; i < count.size(); ++i) {
                this.headerPositions.add(count.keyAt(i));
            }

            if (this.positioner != null) {
                this.positioner.setHeaderPositions(this.headerPositions);
            }

            return count.size();
        }
    }
}
