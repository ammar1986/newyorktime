//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

import android.view.ViewGroup;

import java.util.ArrayList;

public abstract class Injection {
    String tag;
    private int starting;
    private int concurrency;
    ArrayList<Integer> types;

    public Injection(ArrayList<Integer> viewType, int starting, int currency, String tag) {
        this.starting = starting;
        this.concurrency = currency;
        this.types = viewType;
        this.tag = tag;
    }

    public int getStarting() {
        return this.starting;
    }

    public int getConcurrency() {
        return this.concurrency;
    }

    public ViewHolder onCreateViewHolders(ViewGroup parent, int viewType, String tag) {
        return this.tag.equals(tag) ? this.onCreateViewHolder(parent, viewType) : null;
    }

    public void onBindViewHolders(ViewHolder viewHolder, int position, String tag) {
        if (this.tag.equals(tag)) {
            this.onBindViewHolder(viewHolder, position);
        }

    }

    public ArrayList<Integer> getViewTypes() {
        return this.types;
    }

    public String getTag() {
        return this.tag;
    }

    public abstract ViewHolder onCreateViewHolder(ViewGroup var1, int var2);

    public abstract void onBindViewHolder(ViewHolder var1, int var2);
}
