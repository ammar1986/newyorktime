/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.tf961.newyorktimes.base;

import androidx.databinding.ViewDataBinding;

import com.tf961.newyorktimes.libraries.happiestrecyclerview.ViewHolder;


/**
 * Created by assaad on 11/07/17.
 */

public class BaseViewHolder<T extends ViewDataBinding, V> extends ViewHolder {

    protected T mViewDataBinding;
    protected V objectRow;

    public BaseViewHolder(T binding) {
        super(binding.getRoot());
        this.mViewDataBinding = binding;
    }

    public T getBinding() {
        return mViewDataBinding;
    }

    public void setData(V object) {
        this.objectRow = object;
        mViewDataBinding.setVariable(com.tf961.newyorktimes.BR.viewModel, object);
        mViewDataBinding.executePendingBindings();
    }
}
