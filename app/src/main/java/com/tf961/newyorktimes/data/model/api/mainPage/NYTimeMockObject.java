package com.tf961.newyorktimes.data.model.api.mainPage;

import android.text.TextUtils;


import org.parceler.Generated;
import org.parceler.Parcel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Generated("org.jsonschema2pojo")
@Parcel(Parcel.Serialization.BEAN)
public class NYTimeMockObject extends Result implements NYTimeInterface {

    @Override
    public String getNYDate() {
        return getPublishedDate();
    }

    @Override
    public String getNYTitle() {
        return getTitle();
    }

    @Override
    public String getNYImage() {
        if (getMedia().isEmpty())
            return "";
        else if (!getMedia().get(0).getMediaMetadata().isEmpty()) {
            String url = getMedia().get(0).getMediaMetadata().get(0).getUrl();
            if (TextUtils.isEmpty(url)) {
                return "";
            } else
                return url;
        } else {
            return "";
        }
    }

    @Override
    public String getNYDescription() {
        return getByline();
    }

    public Long getTimeInMillisecondsFromDate(String givenDateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        long timeInMilliseconds = 0L;
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

}
