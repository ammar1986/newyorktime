/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.tf961.newyorktimes.base;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;
import com.tf961.newyorktimes.ViewModelProviderFactory;
import com.tf961.newyorktimes.data.AppDataManager;
import com.tf961.newyorktimes.data.local.prefs.AppPreferencesHelper;
import com.tf961.newyorktimes.data.remote.ApiHeader;
import com.tf961.newyorktimes.data.remote.AppApiHelper;
import com.tf961.newyorktimes.libraries.HappyDB.HappyDB;
import com.tf961.newyorktimes.utils.rx.AppSchedulerProvider;




/**
 * "notification": {
 * "body": "body test",
 * "title": "title test"
 * },
 * Created by assaad on 07/07/17.
 */

public abstract class BaseActivity<T extends ViewDataBinding, V extends BaseViewModel> extends AppCompatActivity
        implements BaseFragment.Callback {

     boolean isForeground = false;


    public void onPrefChanger(String key){

    }




    SharedPreferences.OnSharedPreferenceChangeListener sharedPreferenceChangeListenerGlobal;

    private void initPrefListener() {
        sharedPreferenceChangeListenerGlobal = (prefs, key) -> {
            onPrefChanger(key);
        };
        HappyDB.getInstance().registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListenerGlobal);
    }

    // TODO
    // this can probably depend on isLoading variable of BaseViewModel,
    // since its going to be common for all the activities
    private ProgressDialog mProgressDialog;
    private T mViewDataBinding;
    private V mViewModel;

    public ViewModelProviderFactory factory;
    /**
     * Override for set binding variable
     *
     * @return variable id
     */


    /**
     * @return layout resource id
     */
    public abstract
    @LayoutRes
    int getLayoutId();

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    public abstract V getViewModel();

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }


    public String getStringResource(int id) {
        return getString(id);
    }


    @Override
    protected void onCreate(@androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        factory = new ViewModelProviderFactory(AppDataManager.getInstance(this,
                new AppPreferencesHelper(this, "PREF_NAME"),
                AppApiHelper.getInstance(ApiHeader.getInstance(new ApiHeader.PublicApiHeader(""), new ApiHeader.ProtectedApiHeader("", 0L, ""))),
                new Gson()), new AppSchedulerProvider());
        performDataBinding();
        initPrefListener();
    }



    @Override
    protected void onPause() {
        super.onPause();
        this.isForeground = false;
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }

    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }








    public  void navigateTo(Integer navigation){};

    private void performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        this.mViewModel = mViewModel == null ? getViewModel() : mViewModel;
        mViewDataBinding.setVariable(com.tf961.newyorktimes.BR.viewModel, mViewModel);
        mViewDataBinding.executePendingBindings();
        mViewDataBinding.setLifecycleOwner(this);

        this.mViewModel.navigatorLiveDate.observe(this, (Observer<Integer>) this::navigateTo);

    }



}

