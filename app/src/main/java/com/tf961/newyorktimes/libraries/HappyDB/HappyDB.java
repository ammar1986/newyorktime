//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.HappyDB;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.tf961.newyorktimes.libraries.HappyDB.encryption.Encryptor;

import org.json.JSONArray;

import java.util.ArrayList;


public class HappyDB {
    private String PREFS_NAME;
    private boolean isEncryptionEnabled = false;
    private String encryptionKey;
    private final SharedPreferences mSharedPreferences;
    Context mContext;
    static HappyDB happyDB;

    public HappyDB(String dataBaseName, Context context) {
        this.PREFS_NAME = dataBaseName;
        this.mContext = context;
        this.mSharedPreferences = context.getSharedPreferences(this.PREFS_NAME, 0);
    }

    public static void init(Context context, String dataBaseName) {
        happyDB = new HappyDB(dataBaseName, context);
    }

    public static HappyDB getInstance() {
        return happyDB;
    }

    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener sharedPreferenceChangeListener) {
        this.mSharedPreferences.registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);
    }

    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener sharedPreferenceChangeListener) {
        this.mSharedPreferences.unregisterOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);
    }

    public SharedPreferences getSharedPreferences() {
        return this.mSharedPreferences;
    }

    public <T> T getArrayList(String key, TypeToken<T> typeToken) {
        return this.getPrefArraylist(key, this.mContext, typeToken);
    }

    public <T> ArrayList<T> getArrayList(String key, Class<T> classOfT) {
        return this.getPrefArraylist(key, this.mContext, classOfT);
    }

    public boolean putInt(int value, String key) {
        return this.savePre(this.mContext, String.valueOf(value), key);
    }

    public int getInt(String key) {
        return this.getPrefInt(this.mContext, key);
    }

    public boolean putBoolean(boolean value, String key) {
        return this.savePre(this.mContext, String.valueOf(value), key);
    }

    public boolean getBoolean(String key) {
        return Boolean.parseBoolean(this.getPref(this.mContext, key));
    }

    public boolean putString(String value, String key) {
        return this.savePre(this.mContext, value, key);
    }

    public String getString(String key) {
        return this.getPref(this.mContext, key);
    }

    public boolean putFloat(float value, String key) {
        return this.savePre(this.mContext, String.valueOf(value), key);
    }

    public float getFloat(String key) {
        return Float.parseFloat(this.getPref(this.mContext, key));
    }

    public boolean putLong(Long value, String key) {
        return this.savePre(this.mContext, String.valueOf(value), key);
    }

    public Long getLong(String key) {
        try {
            return Long.parseLong(this.getPref(this.mContext, key));
        } catch (Exception var3) {
            return null;
        }
    }

    public boolean putObject(Object obj, String key) {
        return this.savePrefObject(obj, key, this.mContext);
    }

    public <T> T getObject(String key, Class<T> myClass) {
        return this.getPrefObject(key, this.mContext, myClass);
    }

    public boolean putArrayOfObject(ArrayList<?> value, String key) {
        return this.savePrefObject(value, key, this.mContext);
    }

    public boolean destroy() {
        Editor editor = this.mSharedPreferences.edit();
        editor.clear();
        editor.commit();
        return true;
    }

    public void setEncryptionKey(String encryptionKey) {
        if (encryptionKey != null) {
            if (encryptionKey.length() < 16) {
                Toast.makeText(this.mContext, "EncryptionKey must be 16 characters", Toast.LENGTH_LONG).show();
                return;
            }

            this.isEncryptionEnabled = true;
        } else {
            this.isEncryptionEnabled = false;
        }

        this.encryptionKey = encryptionKey;
    }

    private <T> T getPrefObject(String Key, Context context, Class<T> myClass) {
        Object value = null;

        try {
            Gson gson = new Gson();
            String json = this.mSharedPreferences.getString(Key, "");
            if (this.isEncryptionEnabled) {
                json = Encryptor.decrypt(this.getEncryptionSP(), json);
            }

            if (json.length() > 0) {
                value = gson.fromJson(json, myClass);
            }
        } catch (Exception var7) {
        }

        return (T) value;
    }

    private <T> T getPrefArraylist(String Key, Context context, TypeToken type) {
        Object list = null;

        try {
            Gson gson = new Gson();
            String json = this.mSharedPreferences.getString(Key, (String)null);
            if (json != null) {
                list = gson.fromJson(json, type.getType());
            }
        } catch (Exception var7) {
        }

        return (T) list;
    }

    private <T> ArrayList<T> getPrefArraylist(String Key, Context context, Class<T> classOfT) {
        ArrayList list = null;

        try {
            Gson gson = (new GsonBuilder()).excludeFieldsWithoutExposeAnnotation().excludeFieldsWithModifiers(new int[]{128}).create();
            String json = this.mSharedPreferences.getString(Key, (String)null);
            if (json != null) {
                JSONArray jsonObj = new JSONArray(json);
                list = new ArrayList();

                for(int i = 0; i < jsonObj.length(); ++i) {
                    list.add(gson.fromJson(jsonObj.get(i).toString(), classOfT));
                }
            }
        } catch (Exception var9) {
        }

        return list;
    }

    private boolean savePrefObject(Object obj, String Key, Context context) {
        if (obj == null) {
            return false;
        } else {
            Editor prefsEditor = this.mSharedPreferences.edit();
            Gson gson = new Gson();
            String json = gson.toJson(obj);
            if (this.isEncryptionEnabled) {
                try {
                    if (json.length() > 0) {
                        json = Encryptor.encrypt(this.getEncryptionSP(), json);
                    }
                } catch (Exception var8) {
                    var8.printStackTrace();
                }
            }

            prefsEditor.putString(Key, json);
            prefsEditor.commit();
            return true;
        }
    }

    private boolean savePre(Context c, String value, String key) {
        if (value == null) {
            return false;
        } else {
            if (this.isEncryptionEnabled) {
                try {
                    if (value.length() > 0) {
                        value = Encryptor.encrypt(this.getEncryptionSP(), value);
                    }
                } catch (Exception var5) {
                    var5.printStackTrace();
                }
            }

            Editor editor = this.mSharedPreferences.edit();
            editor.putString(key, value);
            editor.commit();
            return true;
        }
    }

    private String getEncryptionSP() {
        String prefEncKey = "";

        try {
            prefEncKey = "3ZLRCTaW8bc9d52W".substring(1, 7) + "3ZLRCTaW8bc9d52W".substring(3, 8) + "3ZLRCTaW8bc9d52W".substring(1, 3) + "3ZLRCTaW8bc9d52W".substring(7, 10);
        } catch (Exception var3) {
            var3.printStackTrace();
        }

        return prefEncKey;
    }

    private String getPref(Context c, String STOTREDkey) {
        String mobileNum = "";

        try {
            mobileNum = this.mSharedPreferences.getString(STOTREDkey, "");
            if (mobileNum.length() > 0 && this.isEncryptionEnabled) {
                mobileNum = Encryptor.decrypt(this.getEncryptionSP(), mobileNum);
            }
        } catch (Exception var5) {
        }

        return mobileNum;
    }

    private int getPrefInt(Context c, String STOTREDkey) {
        int num = -1;

        try {
            String mobileNum = this.mSharedPreferences.getString(STOTREDkey, "");
            if (this.isEncryptionEnabled) {
                mobileNum = Encryptor.decrypt(this.getEncryptionSP(), mobileNum);
            }

            num = Integer.parseInt(mobileNum);
        } catch (Exception var5) {
        }

        return num;
    }
}
