//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView.LayoutParams;

import static android.view.View.VISIBLE;

class FooterHolder extends ViewHolder {
    public FooterHolder(View itemView) {
        super(itemView);
    }

    public void setVisibility(boolean visibility) {
        LayoutParams param = (LayoutParams)this.itemView.getLayoutParams();
        if (visibility) {
            param.height = -2;
            param.width = -1;
            this.itemView.setVisibility(VISIBLE);
        } else {
            this.itemView.setVisibility(View.GONE);
            param.height = 0;
            param.width = 0;
        }

        this.itemView.setLayoutParams(param);
    }


}
