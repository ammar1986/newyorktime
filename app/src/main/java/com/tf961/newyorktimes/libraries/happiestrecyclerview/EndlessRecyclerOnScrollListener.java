//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView.LayoutManager;
import androidx.recyclerview.widget.RecyclerView.OnScrollListener;
import androidx.recyclerview.widget.RecyclerView;
public abstract class EndlessRecyclerOnScrollListener extends OnScrollListener {
    public static String TAG = EndlessRecyclerOnScrollListener.class.getSimpleName();
    protected int previousTotal = 0;
    protected boolean loading = true;
    protected boolean moreDataAvailable = false;
    private int visibleThreshold = 3;
    int firstVisibleItem;
    int visibleItemCount;
    int totalItemCount;
    protected int current_page = 1;
    private LinearLayoutManager mLinearLayoutManager;
    private GridLayoutManager mGridLayoutManager;
    private int layoutType;
    private static final int GRID_TYPE = 0;
    private static final int LINEAR_TYPE = 1;

    public EndlessRecyclerOnScrollListener(LayoutManager layoutManager) {
        if (layoutManager instanceof LinearLayoutManager) {
            this.mLinearLayoutManager = (LinearLayoutManager)layoutManager;
            this.layoutType = 1;
            this.visibleThreshold = 2;
        }

        if (layoutManager instanceof GridLayoutManager) {
            this.mGridLayoutManager = (GridLayoutManager)layoutManager;
            this.layoutType = 0;
            this.visibleThreshold = this.mGridLayoutManager.getSpanCount();
        }

    }

    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (dy > 0) {
            switch(this.layoutType) {
            case 0:
                this.visibleItemCount = this.mGridLayoutManager.getChildCount();
                this.totalItemCount = this.mGridLayoutManager.getItemCount() - 1;
                this.firstVisibleItem = this.mGridLayoutManager.findFirstVisibleItemPosition();
                if (this.loading && this.totalItemCount > this.previousTotal) {
                    this.loading = false;
                    this.previousTotal = this.totalItemCount;
                }

                if (!this.loading && this.totalItemCount - this.visibleItemCount <= this.firstVisibleItem + this.visibleThreshold * this.mGridLayoutManager.getSpanCount()) {
                    ++this.current_page;
                    this.moreDataAvailable = true;
                    this.onLoadMore(this.current_page);
                    this.loading = true;
                }
                break;
            case 1:
                this.visibleItemCount = recyclerView.getChildCount();
                this.totalItemCount = this.mLinearLayoutManager.getItemCount() - 1;
                this.firstVisibleItem = this.mLinearLayoutManager.findFirstVisibleItemPosition();
                if (this.loading && this.totalItemCount > this.previousTotal) {
                    this.loading = false;
                    this.previousTotal = this.totalItemCount;
                }

                if (!this.loading && this.totalItemCount - this.visibleItemCount <= this.firstVisibleItem + this.visibleThreshold) {
                    ++this.current_page;
                    this.moreDataAvailable = true;
                    this.onLoadMore(this.current_page);
                    this.loading = true;
                }
            }
        }

    }

    public abstract void onLoadMore(int var1);
}
