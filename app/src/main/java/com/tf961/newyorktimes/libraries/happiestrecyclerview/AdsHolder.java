//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

import android.view.View;
import android.widget.RelativeLayout;

import com.tf961.newyorktimes.R;


public class AdsHolder extends ViewHolder {
    public RelativeLayout relativeLayout;

    public AdsHolder(View itemView) {
        super(itemView);
        this.relativeLayout = (RelativeLayout)itemView.findViewById(R.id.parent_layout);
    }
}
