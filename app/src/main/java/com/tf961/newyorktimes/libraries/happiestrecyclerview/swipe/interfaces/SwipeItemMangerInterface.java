//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.interfaces;

import java.util.List;

import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.SwipeLayout;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe.util.Attributes.Mode;

public interface SwipeItemMangerInterface {
    void openItem(int var1);

    void closeItem(int var1);

    void closeAllExcept(SwipeLayout var1);

    void closeAllItems();

    List<Integer> getOpenItems();

    List<SwipeLayout> getOpenLayouts();

    void removeShownLayouts(SwipeLayout var1);

    boolean isOpen(int var1);

    Mode getMode();

    void setMode(Mode var1);
}
