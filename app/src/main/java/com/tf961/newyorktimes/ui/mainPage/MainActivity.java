package com.tf961.newyorktimes.ui.mainPage;

import androidx.lifecycle.ViewModelProvider;
import androidx.test.espresso.idling.CountingIdlingResource;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.tf961.newyorktimes.R;
import com.tf961.newyorktimes.databinding.ActivityMainBinding;
import com.tf961.newyorktimes.base.BaseActivity;
import com.tf961.newyorktimes.libraries.happiestrecyclerview.OnItemClickListener;
import com.tf961.newyorktimes.ui.mainPage.list.MainPageAdapter;

public class MainActivity extends BaseActivity<ActivityMainBinding,MainViewModel> {

    public CountingIdlingResource idlingResource = new CountingIdlingResource("Data_Loaded");

    MainViewModel mainViewModel;
    /**
     * @return layout resource id
     */
    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    @Override
    public MainViewModel getViewModel() {
        mainViewModel =  new ViewModelProvider(this, factory).get(MainViewModel.class);
        return mainViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        idlingResource.increment();
        super.onCreate(savedInstanceState);
        getViewDataBinding().recyclerView.setAdapter(new MainPageAdapter(this));
        getViewDataBinding().recyclerView.getAdapter().setOnItemClickedListener(this, 0, new OnItemClickListener() {
            @Override
            public void onItemClick(View var1, int var2) {
                Toast.makeText(MainActivity.this,"test message", Toast.LENGTH_LONG).show();
            }
            @Override
            public void onItemLongClick(View var1, int var2) {

            }
        });
        mainViewModel.liveData.observe(this, nyTimeMockObjects -> {
            getViewDataBinding().recyclerView.getAdapter().setData(nyTimeMockObjects);
            getViewDataBinding().recyclerView.setVisibility(View.VISIBLE);
            idlingResource.decrement();
        });
    }
}