/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.tf961.newyorktimes.ui.mainPage;

import androidx.lifecycle.MutableLiveData;

import com.tf961.newyorktimes.data.DataManager;
import com.tf961.newyorktimes.data.model.api.BaseResponse;
import com.tf961.newyorktimes.data.model.api.mainPage.NYTimeInterface;
import com.tf961.newyorktimes.base.BaseViewModel;
import com.tf961.newyorktimes.utils.rx.SchedulerProvider;

import java.util.ArrayList;


/**
 * Created by assaad on 08/07/17.
 */

public class MainViewModel extends BaseViewModel {


    MutableLiveData<ArrayList<NYTimeInterface>> liveData;
    public MainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        liveData = new MutableLiveData<>();
        getALlData(7);
    }

    private void getALlData(int period){
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .getArticles(period)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .map(BaseResponse::getData)
                .subscribe(response -> {
                    liveData.setValue(new ArrayList<>(response));
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);



                }));
    }


}
