package com.tf961.newyorktimes.libraries.happiestrecyclerview;

public class HeaderObject {

    public boolean isNewHeader;
    String title;
    public HeaderObject(boolean isNewHeader, String title){
        this.isNewHeader = isNewHeader;
        this.title = title;
    }

}
