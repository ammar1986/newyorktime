//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview.swipe;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import androidx.customview.widget.ViewDragHelper;
import androidx.customview.widget.ViewDragHelper.Callback;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.tf961.newyorktimes.R.styleable;

public class SwipeLayout extends FrameLayout {
    /** @deprecated */
    @Deprecated
    public static final int EMPTY_LAYOUT = -1;
    private static final int DRAG_LEFT = 1;
    private static final int DRAG_RIGHT = 2;
    private static final int DRAG_TOP = 4;
    private static final int DRAG_BOTTOM = 8;
    private static final SwipeLayout.DragEdge DefaultDragEdge;
    private int mTouchSlop;
    private SwipeLayout.DragEdge mCurrentDragEdge;
    private ViewDragHelper mDragHelper;
    private int mDragDistance;
    private LinkedHashMap<DragEdge, View> mDragEdges;
    private SwipeLayout.ShowMode mShowMode;
    private float[] mEdgeSwipesOffset;
    private List<SwipeListener> mSwipeListeners;
    private List<SwipeDenier> mSwipeDeniers;
    private Map<View, ArrayList<OnRevealListener>> mRevealListeners;
    private Map<View, Boolean> mShowEntirely;
    private SwipeLayout.DoubleClickListener mDoubleClickListener;
    private boolean mSwipeEnabled;
    private boolean[] mSwipesEnabled;
    private boolean mClickToClose;
    private Callback mDragHelperCallback;
    private int mEventCounter;
    private List<OnLayout> mOnLayoutListeners;
    private boolean mIsBeingDragged;
    private float sX;
    private float sY;
    OnClickListener clickListener;
    OnLongClickListener longClickListener;
    private Rect hitSurfaceRect;
    private GestureDetector gestureDetector;

    public SwipeLayout(Context context) {
        this(context, (AttributeSet)null);
    }

    public SwipeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SwipeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mCurrentDragEdge = DefaultDragEdge;
        this.mDragDistance = 0;
        this.mDragEdges = new LinkedHashMap();
        this.mEdgeSwipesOffset = new float[4];
        this.mSwipeListeners = new ArrayList();
        this.mSwipeDeniers = new ArrayList();
        this.mRevealListeners = new HashMap();
        this.mShowEntirely = new HashMap();
        this.mSwipeEnabled = true;
        this.mSwipesEnabled = new boolean[]{true, true, true, true};
        this.mClickToClose = false;
        this.mDragHelperCallback = new Callback() {
            boolean isCloseBeforeDrag = true;

            public int clampViewPositionHorizontal(View child, int left, int dx) {
                if (child == SwipeLayout.this.getSurfaceView()) {
                    switch(SwipeLayout.this.mCurrentDragEdge) {
                    case Top:
                    case Bottom:
                        return SwipeLayout.this.getPaddingLeft();
                    case Left:
                        if (left < SwipeLayout.this.getPaddingLeft()) {
                            return SwipeLayout.this.getPaddingLeft();
                        }

                        if (left > SwipeLayout.this.getPaddingLeft() + SwipeLayout.this.mDragDistance) {
                            return SwipeLayout.this.getPaddingLeft() + SwipeLayout.this.mDragDistance;
                        }
                        break;
                    case Right:
                        if (left > SwipeLayout.this.getPaddingLeft()) {
                            return SwipeLayout.this.getPaddingLeft();
                        }

                        if (left < SwipeLayout.this.getPaddingLeft() - SwipeLayout.this.mDragDistance) {
                            return SwipeLayout.this.getPaddingLeft() - SwipeLayout.this.mDragDistance;
                        }
                    }
                } else if (SwipeLayout.this.getCurrentBottomView() == child) {
                    switch(SwipeLayout.this.mCurrentDragEdge) {
                    case Top:
                    case Bottom:
                        return SwipeLayout.this.getPaddingLeft();
                    case Left:
                        if (SwipeLayout.this.mShowMode == SwipeLayout.ShowMode.PullOut && left > SwipeLayout.this.getPaddingLeft()) {
                            return SwipeLayout.this.getPaddingLeft();
                        }
                        break;
                    case Right:
                        if (SwipeLayout.this.mShowMode == SwipeLayout.ShowMode.PullOut && left < SwipeLayout.this.getMeasuredWidth() - SwipeLayout.this.mDragDistance) {
                            return SwipeLayout.this.getMeasuredWidth() - SwipeLayout.this.mDragDistance;
                        }
                    }
                }

                return left;
            }

            public int clampViewPositionVertical(View child, int top, int dy) {
                if (child == SwipeLayout.this.getSurfaceView()) {
                    switch(SwipeLayout.this.mCurrentDragEdge) {
                    case Top:
                        if (top < SwipeLayout.this.getPaddingTop()) {
                            return SwipeLayout.this.getPaddingTop();
                        }

                        if (top > SwipeLayout.this.getPaddingTop() + SwipeLayout.this.mDragDistance) {
                            return SwipeLayout.this.getPaddingTop() + SwipeLayout.this.mDragDistance;
                        }
                        break;
                    case Bottom:
                        if (top < SwipeLayout.this.getPaddingTop() - SwipeLayout.this.mDragDistance) {
                            return SwipeLayout.this.getPaddingTop() - SwipeLayout.this.mDragDistance;
                        }

                        if (top > SwipeLayout.this.getPaddingTop()) {
                            return SwipeLayout.this.getPaddingTop();
                        }
                        break;
                    case Left:
                    case Right:
                        return SwipeLayout.this.getPaddingTop();
                    }
                } else {
                    View surfaceView = SwipeLayout.this.getSurfaceView();
                    int surfaceViewTop = surfaceView == null ? 0 : surfaceView.getTop();
                    switch(SwipeLayout.this.mCurrentDragEdge) {
                    case Top:
                        if (SwipeLayout.this.mShowMode == SwipeLayout.ShowMode.PullOut) {
                            if (top > SwipeLayout.this.getPaddingTop()) {
                                return SwipeLayout.this.getPaddingTop();
                            }
                        } else {
                            if (surfaceViewTop + dy < SwipeLayout.this.getPaddingTop()) {
                                return SwipeLayout.this.getPaddingTop();
                            }

                            if (surfaceViewTop + dy > SwipeLayout.this.getPaddingTop() + SwipeLayout.this.mDragDistance) {
                                return SwipeLayout.this.getPaddingTop() + SwipeLayout.this.mDragDistance;
                            }
                        }
                        break;
                    case Bottom:
                        if (SwipeLayout.this.mShowMode == SwipeLayout.ShowMode.PullOut) {
                            if (top < SwipeLayout.this.getMeasuredHeight() - SwipeLayout.this.mDragDistance) {
                                return SwipeLayout.this.getMeasuredHeight() - SwipeLayout.this.mDragDistance;
                            }
                        } else {
                            if (surfaceViewTop + dy >= SwipeLayout.this.getPaddingTop()) {
                                return SwipeLayout.this.getPaddingTop();
                            }

                            if (surfaceViewTop + dy <= SwipeLayout.this.getPaddingTop() - SwipeLayout.this.mDragDistance) {
                                return SwipeLayout.this.getPaddingTop() - SwipeLayout.this.mDragDistance;
                            }
                        }
                        break;
                    case Left:
                    case Right:
                        return SwipeLayout.this.getPaddingTop();
                    }
                }

                return top;
            }

            public boolean tryCaptureView(View child, int pointerId) {
                boolean result = child == SwipeLayout.this.getSurfaceView() || SwipeLayout.this.getBottomViews().contains(child);
                if (result) {
                    this.isCloseBeforeDrag = SwipeLayout.this.getOpenStatus() == SwipeLayout.Status.Close;
                }

                return result;
            }

            public int getViewHorizontalDragRange(View child) {
                return SwipeLayout.this.mDragDistance;
            }

            public int getViewVerticalDragRange(View child) {
                return SwipeLayout.this.mDragDistance;
            }

            public void onViewReleased(View releasedChild, float xvel, float yvel) {
                super.onViewReleased(releasedChild, xvel, yvel);
                Iterator var4 = SwipeLayout.this.mSwipeListeners.iterator();

                while(var4.hasNext()) {
                    SwipeLayout.SwipeListener l = (SwipeLayout.SwipeListener)var4.next();
                    l.onHandRelease(SwipeLayout.this, xvel, yvel);
                }

                SwipeLayout.this.processHandRelease(xvel, yvel, this.isCloseBeforeDrag);
                SwipeLayout.this.invalidate();
            }

            public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
                View surfaceView = SwipeLayout.this.getSurfaceView();
                if (surfaceView != null) {
                    View currentBottomView = SwipeLayout.this.getCurrentBottomView();
                    int evLeft = surfaceView.getLeft();
                    int evRight = surfaceView.getRight();
                    int evTop = surfaceView.getTop();
                    int evBottom = surfaceView.getBottom();
                    if (changedView == surfaceView) {
                        if (SwipeLayout.this.mShowMode == SwipeLayout.ShowMode.PullOut && currentBottomView != null) {
                            if (SwipeLayout.this.mCurrentDragEdge != SwipeLayout.DragEdge.Left && SwipeLayout.this.mCurrentDragEdge != SwipeLayout.DragEdge.Right) {
                                currentBottomView.offsetTopAndBottom(dy);
                            } else {
                                currentBottomView.offsetLeftAndRight(dx);
                            }
                        }
                    } else if (SwipeLayout.this.getBottomViews().contains(changedView)) {
                        if (SwipeLayout.this.mShowMode == SwipeLayout.ShowMode.PullOut) {
                            surfaceView.offsetLeftAndRight(dx);
                            surfaceView.offsetTopAndBottom(dy);
                        } else {
                            Rect rect = SwipeLayout.this.computeBottomLayDown(SwipeLayout.this.mCurrentDragEdge);
                            if (currentBottomView != null) {
                                currentBottomView.layout(rect.left, rect.top, rect.right, rect.bottom);
                            }

                            int newLeft = surfaceView.getLeft() + dx;
                            int newTop = surfaceView.getTop() + dy;
                            if (SwipeLayout.this.mCurrentDragEdge == SwipeLayout.DragEdge.Left && newLeft < SwipeLayout.this.getPaddingLeft()) {
                                newLeft = SwipeLayout.this.getPaddingLeft();
                            } else if (SwipeLayout.this.mCurrentDragEdge == SwipeLayout.DragEdge.Right && newLeft > SwipeLayout.this.getPaddingLeft()) {
                                newLeft = SwipeLayout.this.getPaddingLeft();
                            } else if (SwipeLayout.this.mCurrentDragEdge == SwipeLayout.DragEdge.Top && newTop < SwipeLayout.this.getPaddingTop()) {
                                newTop = SwipeLayout.this.getPaddingTop();
                            } else if (SwipeLayout.this.mCurrentDragEdge == SwipeLayout.DragEdge.Bottom && newTop > SwipeLayout.this.getPaddingTop()) {
                                newTop = SwipeLayout.this.getPaddingTop();
                            }

                            surfaceView.layout(newLeft, newTop, newLeft + SwipeLayout.this.getMeasuredWidth(), newTop + SwipeLayout.this.getMeasuredHeight());
                        }
                    }

                    SwipeLayout.this.dispatchRevealEvent(evLeft, evTop, evRight, evBottom);
                    SwipeLayout.this.dispatchSwipeEvent(evLeft, evTop, dx, dy);
                    SwipeLayout.this.invalidate();
                }
            }
        };
        this.mEventCounter = 0;
        this.sX = -1.0F;
        this.sY = -1.0F;
        this.gestureDetector = new GestureDetector(this.getContext(), new SwipeLayout.SwipeDetector());
        this.mDragHelper = ViewDragHelper.create(this, this.mDragHelperCallback);
        this.mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        TypedArray a = context.obtainStyledAttributes(attrs, styleable.SwipeLayout);
        int dragEdgeChoices = a.getInt(styleable.SwipeLayout_drag_edge, 2);
        this.mEdgeSwipesOffset[SwipeLayout.DragEdge.Left.ordinal()] = a.getDimension(styleable.SwipeLayout_leftEdgeSwipeOffset, 0.0F);
        this.mEdgeSwipesOffset[SwipeLayout.DragEdge.Right.ordinal()] = a.getDimension(styleable.SwipeLayout_rightEdgeSwipeOffset, 0.0F);
        this.mEdgeSwipesOffset[SwipeLayout.DragEdge.Top.ordinal()] = a.getDimension(styleable.SwipeLayout_topEdgeSwipeOffset, 0.0F);
        this.mEdgeSwipesOffset[SwipeLayout.DragEdge.Bottom.ordinal()] = a.getDimension(styleable.SwipeLayout_bottomEdgeSwipeOffset, 0.0F);
        this.setClickToClose(a.getBoolean(styleable.SwipeLayout_clickToClose, this.mClickToClose));
        if ((dragEdgeChoices & DRAG_LEFT) == DRAG_LEFT) {
            mDragEdges.put(SwipeLayout.DragEdge.Left, null);
        }
        if ((dragEdgeChoices & DRAG_TOP) == DRAG_TOP) {
            mDragEdges.put(SwipeLayout.DragEdge.Top, null);
        }
        if ((dragEdgeChoices & DRAG_RIGHT) == DRAG_RIGHT) {
            mDragEdges.put(SwipeLayout.DragEdge.Right, null);
        }
        if ((dragEdgeChoices & DRAG_BOTTOM) == DRAG_BOTTOM) {
            mDragEdges.put(SwipeLayout.DragEdge.Bottom, null);
        }

        int ordinal = a.getInt(styleable.SwipeLayout_show_mode, SwipeLayout.ShowMode.PullOut.ordinal());
        this.mShowMode = SwipeLayout.ShowMode.values()[ordinal];
        a.recycle();
    }

    public void addSwipeListener(SwipeLayout.SwipeListener l) {
        this.mSwipeListeners.add(l);
    }

    public void removeSwipeListener(SwipeLayout.SwipeListener l) {
        this.mSwipeListeners.remove(l);
    }

    public void addSwipeDenier(SwipeLayout.SwipeDenier denier) {
        this.mSwipeDeniers.add(denier);
    }

    public void removeSwipeDenier(SwipeLayout.SwipeDenier denier) {
        this.mSwipeDeniers.remove(denier);
    }

    public void removeAllSwipeDeniers() {
        this.mSwipeDeniers.clear();
    }

    public void addRevealListener(int childId, SwipeLayout.OnRevealListener l) {
        View child = this.findViewById(childId);
        if (child == null) {
            throw new IllegalArgumentException("Child does not belong to SwipeListener.");
        } else {
            if (!this.mShowEntirely.containsKey(child)) {
                this.mShowEntirely.put(child, false);
            }

            if (this.mRevealListeners.get(child) == null) {
                this.mRevealListeners.put(child, new ArrayList());
            }

            ((ArrayList)this.mRevealListeners.get(child)).add(l);
        }
    }

    public void addRevealListener(int[] childIds, SwipeLayout.OnRevealListener l) {
        int[] var3 = childIds;
        int var4 = childIds.length;

        for(int var5 = 0; var5 < var4; ++var5) {
            int i = var3[var5];
            this.addRevealListener(i, l);
        }

    }

    public void removeRevealListener(int childId, SwipeLayout.OnRevealListener l) {
        View child = this.findViewById(childId);
        if (child != null) {
            this.mShowEntirely.remove(child);
            if (this.mRevealListeners.containsKey(child)) {
                ((ArrayList)this.mRevealListeners.get(child)).remove(l);
            }

        }
    }

    public void removeAllRevealListeners(int childId) {
        View child = this.findViewById(childId);
        if (child != null) {
            this.mRevealListeners.remove(child);
            this.mShowEntirely.remove(child);
        }

    }

    protected boolean isViewTotallyFirstShowed(View child, Rect relativePosition, SwipeLayout.DragEdge edge, int surfaceLeft, int surfaceTop, int surfaceRight, int surfaceBottom) {
        if ((Boolean)this.mShowEntirely.get(child)) {
            return false;
        } else {
            int childLeft = relativePosition.left;
            int childRight = relativePosition.right;
            int childTop = relativePosition.top;
            int childBottom = relativePosition.bottom;
            boolean r = false;
            if (this.getShowMode() == SwipeLayout.ShowMode.LayDown) {
                if (edge == SwipeLayout.DragEdge.Right && surfaceRight <= childLeft || edge == SwipeLayout.DragEdge.Left && surfaceLeft >= childRight || edge == SwipeLayout.DragEdge.Top && surfaceTop >= childBottom || edge == SwipeLayout.DragEdge.Bottom && surfaceBottom <= childTop) {
                    r = true;
                }
            } else if (this.getShowMode() == SwipeLayout.ShowMode.PullOut && (edge == SwipeLayout.DragEdge.Right && childRight <= this.getWidth() || edge == SwipeLayout.DragEdge.Left && childLeft >= this.getPaddingLeft() || edge == SwipeLayout.DragEdge.Top && childTop >= this.getPaddingTop() || edge == SwipeLayout.DragEdge.Bottom && childBottom <= this.getHeight())) {
                r = true;
            }

            return r;
        }
    }

    protected boolean isViewShowing(View child, Rect relativePosition, SwipeLayout.DragEdge availableEdge, int surfaceLeft, int surfaceTop, int surfaceRight, int surfaceBottom) {
        int childLeft = relativePosition.left;
        int childRight = relativePosition.right;
        int childTop = relativePosition.top;
        int childBottom = relativePosition.bottom;
        if (this.getShowMode() == SwipeLayout.ShowMode.LayDown) {
            switch(availableEdge) {
            case Top:
                if (surfaceTop >= childTop && surfaceTop < childBottom) {
                    return true;
                }
                break;
            case Bottom:
                if (surfaceBottom > childTop && surfaceBottom <= childBottom) {
                    return true;
                }
                break;
            case Left:
                if (surfaceLeft < childRight && surfaceLeft >= childLeft) {
                    return true;
                }
                break;
            case Right:
                if (surfaceRight > childLeft && surfaceRight <= childRight) {
                    return true;
                }
            }
        } else if (this.getShowMode() == SwipeLayout.ShowMode.PullOut) {
            switch(availableEdge) {
            case Top:
                if (childTop < this.getPaddingTop() && childBottom >= this.getPaddingTop()) {
                    return true;
                }
                break;
            case Bottom:
                if (childTop < this.getHeight() && childTop >= this.getPaddingTop()) {
                    return true;
                }
                break;
            case Left:
                if (childRight >= this.getPaddingLeft() && childLeft < this.getPaddingLeft()) {
                    return true;
                }
                break;
            case Right:
                if (childLeft <= this.getWidth() && childRight > this.getWidth()) {
                    return true;
                }
            }
        }

        return false;
    }

    protected Rect getRelativePosition(View child) {
        View t = child;

        Rect r;
        for(r = new Rect(child.getLeft(), child.getTop(), 0, 0); t.getParent() != null && t != this.getRootView(); r.top += t.getTop()) {
            t = (View)t.getParent();
            if (t == this) {
                break;
            }

            r.left += t.getLeft();
        }

        r.right = r.left + child.getMeasuredWidth();
        r.bottom = r.top + child.getMeasuredHeight();
        return r;
    }

    protected void dispatchSwipeEvent(int surfaceLeft, int surfaceTop, int dx, int dy) {
        SwipeLayout.DragEdge edge = this.getDragEdge();
        boolean open = true;
        if (edge == SwipeLayout.DragEdge.Left) {
            if (dx < 0) {
                open = false;
            }
        } else if (edge == SwipeLayout.DragEdge.Right) {
            if (dx > 0) {
                open = false;
            }
        } else if (edge == SwipeLayout.DragEdge.Top) {
            if (dy < 0) {
                open = false;
            }
        } else if (edge == SwipeLayout.DragEdge.Bottom && dy > 0) {
            open = false;
        }

        this.dispatchSwipeEvent(surfaceLeft, surfaceTop, open);
    }

    protected void dispatchSwipeEvent(int surfaceLeft, int surfaceTop, boolean open) {
        this.safeBottomView();
        SwipeLayout.Status status = this.getOpenStatus();
        if (!this.mSwipeListeners.isEmpty()) {
            ++this.mEventCounter;

            Iterator var5;
            SwipeLayout.SwipeListener l;
            for(var5 = this.mSwipeListeners.iterator(); var5.hasNext(); l.onUpdate(this, surfaceLeft - this.getPaddingLeft(), surfaceTop - this.getPaddingTop())) {
                l = (SwipeLayout.SwipeListener)var5.next();
                if (this.mEventCounter == 1) {
                    if (open) {
                        l.onStartOpen(this);
                    } else {
                        l.onStartClose(this);
                    }
                }
            }

            if (status == SwipeLayout.Status.Close) {
                var5 = this.mSwipeListeners.iterator();

                while(var5.hasNext()) {
                    l = (SwipeLayout.SwipeListener)var5.next();
                    l.onClose(this);
                }

                this.mEventCounter = 0;
            }

            if (status == SwipeLayout.Status.Open) {
                View currentBottomView = this.getCurrentBottomView();
                if (currentBottomView != null) {
                    currentBottomView.setEnabled(true);
                }

                Iterator var9 = this.mSwipeListeners.iterator();

                while(var9.hasNext()) {
                    SwipeLayout.SwipeListener l1 = (SwipeLayout.SwipeListener)var9.next();
                    l1.onOpen(this);
                }

                this.mEventCounter = 0;
            }
        }

    }

    private void safeBottomView() {
        SwipeLayout.Status status = this.getOpenStatus();
        List<View> bottoms = this.getBottomViews();
        if (status == SwipeLayout.Status.Close) {
            Iterator var3 = bottoms.iterator();

            while(var3.hasNext()) {
                View bottom = (View)var3.next();
                if (bottom != null && bottom.getVisibility() != INVISIBLE) {
                    setVisibility(INVISIBLE);
                }
            }
        } else {
            View currentBottomView = this.getCurrentBottomView();
            if (currentBottomView != null && currentBottomView.getVisibility() != VISIBLE) {
                currentBottomView.setVisibility(VISIBLE);
            }
        }

    }

    protected void dispatchRevealEvent(int surfaceLeft, int surfaceTop, int surfaceRight, int surfaceBottom) {
        if (!this.mRevealListeners.isEmpty()) {
            Iterator var5 = this.mRevealListeners.entrySet().iterator();

            label74:
            while(true) {
                Entry entry;
                View child;
                Rect rect;
                do {
                    if (!var5.hasNext()) {
                        return;
                    }

                    entry = (Entry)var5.next();
                    child = (View)entry.getKey();
                    rect = this.getRelativePosition(child);
                    if (this.isViewShowing(child, rect, this.mCurrentDragEdge, surfaceLeft, surfaceTop, surfaceRight, surfaceBottom)) {
                        this.mShowEntirely.put(child, false);
                        int distance = 0;
                        float fraction = 0.0F;
                        if (this.getShowMode() == SwipeLayout.ShowMode.LayDown) {
                            switch(this.mCurrentDragEdge) {
                            case Top:
                                distance = rect.top - surfaceTop;
                                fraction = (float)distance / (float)child.getHeight();
                                break;
                            case Bottom:
                                distance = rect.bottom - surfaceBottom;
                                fraction = (float)distance / (float)child.getHeight();
                                break;
                            case Left:
                                distance = rect.left - surfaceLeft;
                                fraction = (float)distance / (float)child.getWidth();
                                break;
                            case Right:
                                distance = rect.right - surfaceRight;
                                fraction = (float)distance / (float)child.getWidth();
                            }
                        } else if (this.getShowMode() == SwipeLayout.ShowMode.PullOut) {
                            switch(this.mCurrentDragEdge) {
                            case Top:
                                distance = rect.bottom - this.getPaddingTop();
                                fraction = (float)distance / (float)child.getHeight();
                                break;
                            case Bottom:
                                distance = rect.top - this.getHeight();
                                fraction = (float)distance / (float)child.getHeight();
                                break;
                            case Left:
                                distance = rect.right - this.getPaddingLeft();
                                fraction = (float)distance / (float)child.getWidth();
                                break;
                            case Right:
                                distance = rect.left - this.getWidth();
                                fraction = (float)distance / (float)child.getWidth();
                            }
                        }

                        Iterator var11 = ((ArrayList)entry.getValue()).iterator();

                        while(var11.hasNext()) {
                            SwipeLayout.OnRevealListener l = (SwipeLayout.OnRevealListener)var11.next();
                            l.onReveal(child, this.mCurrentDragEdge, Math.abs(fraction), distance);
                            if (Math.abs(fraction) == 1.0F) {
                                this.mShowEntirely.put(child, true);
                            }
                        }
                    }
                } while(!this.isViewTotallyFirstShowed(child, rect, this.mCurrentDragEdge, surfaceLeft, surfaceTop, surfaceRight, surfaceBottom));

                this.mShowEntirely.put(child, true);
                Iterator var13 = ((ArrayList)entry.getValue()).iterator();

                while(true) {
                    while(true) {
                        if (!var13.hasNext()) {
                            continue label74;
                        }

                        SwipeLayout.OnRevealListener l = (SwipeLayout.OnRevealListener)var13.next();
                        if (this.mCurrentDragEdge != SwipeLayout.DragEdge.Left && this.mCurrentDragEdge != SwipeLayout.DragEdge.Right) {
                            l.onReveal(child, this.mCurrentDragEdge, 1.0F, child.getHeight());
                        } else {
                            l.onReveal(child, this.mCurrentDragEdge, 1.0F, child.getWidth());
                        }
                    }
                }
            }
        }
    }

    public void computeScroll() {
        super.computeScroll();
        if (this.mDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }

    }

    public void addOnLayoutListener(SwipeLayout.OnLayout l) {
        if (this.mOnLayoutListeners == null) {
            this.mOnLayoutListeners = new ArrayList();
        }

        this.mOnLayoutListeners.add(l);
    }

    public void removeOnLayoutListener(SwipeLayout.OnLayout l) {
        if (this.mOnLayoutListeners != null) {
            this.mOnLayoutListeners.remove(l);
        }

    }

    public void addDrag(SwipeLayout.DragEdge dragEdge, View child) {
        this.addDrag(dragEdge, child, (LayoutParams)null);
    }

    public void addDrag(SwipeLayout.DragEdge dragEdge, View child, ViewGroup.LayoutParams params){
        if(params==null){
            params = generateDefaultLayoutParams();
        }
        if(!checkLayoutParams(params)){
            params = generateLayoutParams(params);
        }
        int gravity = -1;
        switch (dragEdge){
            case Left:gravity = Gravity.LEFT;break;
            case Right:gravity = Gravity.RIGHT;break;
            case Top:gravity = Gravity.TOP;break;
            case Bottom:gravity = Gravity.BOTTOM;break;
        }
        if(params instanceof LayoutParams){
            ((LayoutParams) params).gravity = gravity;
        }
        addView(child, 0, params);
    }

    public void addView(View child, int index, LayoutParams params) {
        int gravity = 0;

        try {
            gravity = (Integer)params.getClass().getField("gravity").get(params);
        } catch (Exception var7) {
            var7.printStackTrace();
        }

        if (gravity > 0) {
            gravity = GravityCompat.getAbsoluteGravity(gravity, ViewCompat.getLayoutDirection(this));
            if ((gravity & 3) == 3) {
                this.mDragEdges.put(SwipeLayout.DragEdge.Left, child);
            }

            if ((gravity & 5) == 5) {
                this.mDragEdges.put(SwipeLayout.DragEdge.Right, child);
            }

            if ((gravity & 48) == 48) {
                this.mDragEdges.put(SwipeLayout.DragEdge.Top, child);
            }

            if ((gravity & 80) == 80) {
                this.mDragEdges.put(SwipeLayout.DragEdge.Bottom, child);
            }
        } else {
            Iterator var5 = this.mDragEdges.entrySet().iterator();

            while(var5.hasNext()) {
                Entry<DragEdge, View> entry = (Entry)var5.next();
                if (entry.getValue() == null) {
                    this.mDragEdges.put(entry.getKey(), child);
                    break;
                }
            }
        }

        if (child != null && child.getParent() != this) {
            super.addView(child, index, params);
        }
    }

    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        this.updateBottomViews();
        if (this.mOnLayoutListeners != null) {
            for(int i = 0; i < this.mOnLayoutListeners.size(); ++i) {
                ((SwipeLayout.OnLayout)this.mOnLayoutListeners.get(i)).onLayout(this);
            }
        }

    }

    void layoutPullOut() {
        Rect rect = this.computeSurfaceLayoutArea(false);
        View surfaceView = this.getSurfaceView();
        if (surfaceView != null) {
            surfaceView.layout(rect.left, rect.top, rect.right, rect.bottom);
            this.bringChildToFront(surfaceView);
        }

        rect = this.computeBottomLayoutAreaViaSurface(SwipeLayout.ShowMode.PullOut, rect);
        View currentBottomView = this.getCurrentBottomView();
        if (currentBottomView != null) {
            currentBottomView.layout(rect.left, rect.top, rect.right, rect.bottom);
        }

    }

    void layoutLayDown() {
        Rect rect = this.computeSurfaceLayoutArea(false);
        View surfaceView = this.getSurfaceView();
        if (surfaceView != null) {
            surfaceView.layout(rect.left, rect.top, rect.right, rect.bottom);
            this.bringChildToFront(surfaceView);
        }

        rect = this.computeBottomLayoutAreaViaSurface(SwipeLayout.ShowMode.LayDown, rect);
        View currentBottomView = this.getCurrentBottomView();
        if (currentBottomView != null) {
            currentBottomView.layout(rect.left, rect.top, rect.right, rect.bottom);
        }

    }

    private void checkCanDrag(MotionEvent ev) {
        if (!this.mIsBeingDragged) {
            if (this.getOpenStatus() == SwipeLayout.Status.Middle) {
                this.mIsBeingDragged = true;
            } else {
                SwipeLayout.Status status = this.getOpenStatus();
                float distanceX = ev.getRawX() - this.sX;
                float distanceY = ev.getRawY() - this.sY;
                float angle = Math.abs(distanceY / distanceX);
                angle = (float)Math.toDegrees(Math.atan((double)angle));
                if (this.getOpenStatus() == SwipeLayout.Status.Close) {
                    SwipeLayout.DragEdge dragEdge;
                    if (angle < 45.0F) {
                        if (distanceX > 0.0F && this.isLeftSwipeEnabled()) {
                            dragEdge = SwipeLayout.DragEdge.Left;
                        } else {
                            if (distanceX >= 0.0F || !this.isRightSwipeEnabled()) {
                                return;
                            }

                            dragEdge = SwipeLayout.DragEdge.Right;
                        }
                    } else if (distanceY > 0.0F && this.isTopSwipeEnabled()) {
                        dragEdge = SwipeLayout.DragEdge.Top;
                    } else {
                        if (distanceY >= 0.0F || !this.isBottomSwipeEnabled()) {
                            return;
                        }

                        dragEdge = SwipeLayout.DragEdge.Bottom;
                    }

                    this.setCurrentDragEdge(dragEdge);
                }

                boolean doNothing = false;
                boolean suitable;
                if (this.mCurrentDragEdge == SwipeLayout.DragEdge.Right) {
                    suitable = status == SwipeLayout.Status.Open && distanceX > (float)this.mTouchSlop || status == SwipeLayout.Status.Close && distanceX < (float)(-this.mTouchSlop);
                    suitable = suitable || status == SwipeLayout.Status.Middle;
                    if (angle > 30.0F || !suitable) {
                        doNothing = true;
                    }
                }

                if (this.mCurrentDragEdge == SwipeLayout.DragEdge.Left) {
                    suitable = status == SwipeLayout.Status.Open && distanceX < (float)(-this.mTouchSlop) || status == SwipeLayout.Status.Close && distanceX > (float)this.mTouchSlop;
                    suitable = suitable || status == SwipeLayout.Status.Middle;
                    if (angle > 30.0F || !suitable) {
                        doNothing = true;
                    }
                }

                if (this.mCurrentDragEdge == SwipeLayout.DragEdge.Top) {
                    suitable = status == SwipeLayout.Status.Open && distanceY < (float)(-this.mTouchSlop) || status == SwipeLayout.Status.Close && distanceY > (float)this.mTouchSlop;
                    suitable = suitable || status == SwipeLayout.Status.Middle;
                    if (angle < 60.0F || !suitable) {
                        doNothing = true;
                    }
                }

                if (this.mCurrentDragEdge == SwipeLayout.DragEdge.Bottom) {
                    suitable = status == SwipeLayout.Status.Open && distanceY > (float)this.mTouchSlop || status == SwipeLayout.Status.Close && distanceY < (float)(-this.mTouchSlop);
                    suitable = suitable || status == SwipeLayout.Status.Middle;
                    if (angle < 60.0F || !suitable) {
                        doNothing = true;
                    }
                }

                this.mIsBeingDragged = !doNothing;
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!this.isSwipeEnabled()) {
            return false;
        } else if (this.mClickToClose && this.getOpenStatus() == SwipeLayout.Status.Open && this.isTouchOnSurface(ev)) {
            return true;
        } else {
            Iterator var2 = this.mSwipeDeniers.iterator();

            SwipeLayout.SwipeDenier denier;
            do {
                if (!var2.hasNext()) {
                    switch(ev.getAction()) {
                    case 0:
                        this.mDragHelper.processTouchEvent(ev);
                        this.mIsBeingDragged = false;
                        this.sX = ev.getRawX();
                        this.sY = ev.getRawY();
                        if (this.getOpenStatus() == SwipeLayout.Status.Middle) {
                            this.mIsBeingDragged = true;
                        }
                        break;
                    case 1:
                    case 3:
                        this.mIsBeingDragged = false;
                        this.mDragHelper.processTouchEvent(ev);
                        break;
                    case 2:
                        boolean beforeCheck = this.mIsBeingDragged;
                        this.checkCanDrag(ev);
                        if (this.mIsBeingDragged) {
                            ViewParent parent = this.getParent();
                            if (parent != null) {
                                parent.requestDisallowInterceptTouchEvent(true);
                            }
                        }

                        if (!beforeCheck && this.mIsBeingDragged) {
                            return false;
                        }
                        break;
                    default:
                        this.mDragHelper.processTouchEvent(ev);
                    }

                    return this.mIsBeingDragged;
                }

                denier = (SwipeLayout.SwipeDenier)var2.next();
            } while(denier == null || !denier.shouldDenySwipe(ev));

            return false;
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!this.isSwipeEnabled()) {
            return super.onTouchEvent(event);
        } else {
            int action = event.getActionMasked();
            this.gestureDetector.onTouchEvent(event);
            switch(action) {
            case 0:
                this.mDragHelper.processTouchEvent(event);
                this.sX = event.getRawX();
                this.sY = event.getRawY();
            case 2:
                this.checkCanDrag(event);
                if (this.mIsBeingDragged) {
                    this.getParent().requestDisallowInterceptTouchEvent(true);
                    this.mDragHelper.processTouchEvent(event);
                }
                break;
            case 1:
            case 3:
                this.mIsBeingDragged = false;
                this.mDragHelper.processTouchEvent(event);
                break;
            default:
                this.mDragHelper.processTouchEvent(event);
            }

            return super.onTouchEvent(event) || this.mIsBeingDragged || action == 0;
        }
    }

    public boolean isClickToClose() {
        return this.mClickToClose;
    }

    public void setClickToClose(boolean mClickToClose) {
        this.mClickToClose = mClickToClose;
    }

    public void setSwipeEnabled(boolean enabled) {
        this.mSwipeEnabled = enabled;
    }

    public boolean isSwipeEnabled() {
        return this.mSwipeEnabled;
    }

    public boolean isLeftSwipeEnabled() {
        View bottomView = (View)this.mDragEdges.get(SwipeLayout.DragEdge.Left);
        return bottomView != null && bottomView.getParent() == this && bottomView != this.getSurfaceView() && this.mSwipesEnabled[SwipeLayout.DragEdge.Left.ordinal()];
    }

    public void setLeftSwipeEnabled(boolean leftSwipeEnabled) {
        this.mSwipesEnabled[SwipeLayout.DragEdge.Left.ordinal()] = leftSwipeEnabled;
    }

    public boolean isRightSwipeEnabled() {
        View bottomView = (View)this.mDragEdges.get(SwipeLayout.DragEdge.Right);
        return bottomView != null && bottomView.getParent() == this && bottomView != this.getSurfaceView() && this.mSwipesEnabled[SwipeLayout.DragEdge.Right.ordinal()];
    }

    public void setRightSwipeEnabled(boolean rightSwipeEnabled) {
        this.mSwipesEnabled[SwipeLayout.DragEdge.Right.ordinal()] = rightSwipeEnabled;
    }

    public boolean isTopSwipeEnabled() {
        View bottomView = (View)this.mDragEdges.get(SwipeLayout.DragEdge.Top);
        return bottomView != null && bottomView.getParent() == this && bottomView != this.getSurfaceView() && this.mSwipesEnabled[SwipeLayout.DragEdge.Top.ordinal()];
    }

    public void setTopSwipeEnabled(boolean topSwipeEnabled) {
        this.mSwipesEnabled[SwipeLayout.DragEdge.Top.ordinal()] = topSwipeEnabled;
    }

    public boolean isBottomSwipeEnabled() {
        View bottomView = (View)this.mDragEdges.get(SwipeLayout.DragEdge.Bottom);
        return bottomView != null && bottomView.getParent() == this && bottomView != this.getSurfaceView() && this.mSwipesEnabled[SwipeLayout.DragEdge.Bottom.ordinal()];
    }

    public void setBottomSwipeEnabled(boolean bottomSwipeEnabled) {
        this.mSwipesEnabled[SwipeLayout.DragEdge.Bottom.ordinal()] = bottomSwipeEnabled;
    }

    private boolean insideAdapterView() {
        return this.getAdapterView() != null;
    }

    private AdapterView getAdapterView() {
        ViewParent t = this.getParent();
        return t instanceof AdapterView ? (AdapterView)t : null;
    }

    private void performAdapterViewItemClick() {
        if (this.getOpenStatus() == SwipeLayout.Status.Close) {
            ViewParent t = this.getParent();
            if (t instanceof AdapterView) {
                AdapterView view = (AdapterView)t;
                int p = view.getPositionForView(this);
                if (p != -1) {
                    view.performItemClick(view.getChildAt(p - view.getFirstVisiblePosition()), p, view.getAdapter().getItemId(p));
                }
            }

        }
    }

    private boolean performAdapterViewItemLongClick() {
        if (this.getOpenStatus() != SwipeLayout.Status.Close) {
            return false;
        } else {
            ViewParent t = this.getParent();
            if (!(t instanceof AdapterView)) {
                return false;
            } else {
                AdapterView view = (AdapterView)t;
                int p = view.getPositionForView(this);
                if (p == -1) {
                    return false;
                } else {
                    long vId = view.getItemIdAtPosition(p);
                    boolean handled = false;

                    try {
                        Method m = AbsListView.class.getDeclaredMethod("performLongPress", View.class, Integer.TYPE, Long.TYPE);
                        m.setAccessible(true);
                        handled = (Boolean)m.invoke(view, this, p, vId);
                    } catch (Exception var8) {
                        var8.printStackTrace();
                        if (view.getOnItemLongClickListener() != null) {
                            handled = view.getOnItemLongClickListener().onItemLongClick(view, this, p, vId);
                        }

                        if (handled) {
                            view.performHapticFeedback(0);
                        }
                    }

                    return handled;
                }
            }
        }
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.insideAdapterView()) {
            if (this.clickListener == null) {
                this.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        SwipeLayout.this.performAdapterViewItemClick();
                    }
                });
            }

            if (this.longClickListener == null) {
                this.setOnLongClickListener(new OnLongClickListener() {
                    public boolean onLongClick(View v) {
                        SwipeLayout.this.performAdapterViewItemLongClick();
                        return true;
                    }
                });
            }
        }

    }

    public void setOnClickListener(OnClickListener l) {
        super.setOnClickListener(l);
        this.clickListener = l;
    }

    public void setOnLongClickListener(OnLongClickListener l) {
        super.setOnLongClickListener(l);
        this.longClickListener = l;
    }

    private boolean isTouchOnSurface(MotionEvent ev) {
        View surfaceView = this.getSurfaceView();
        if (surfaceView == null) {
            return false;
        } else {
            if (this.hitSurfaceRect == null) {
                this.hitSurfaceRect = new Rect();
            }

            surfaceView.getHitRect(this.hitSurfaceRect);
            return this.hitSurfaceRect.contains((int)ev.getX(), (int)ev.getY());
        }
    }

    public void setDragDistance(int max) {
        if (max < 0) {
            max = 0;
        }

        this.mDragDistance = this.dp2px((float)max);
        this.requestLayout();
    }

    public void setShowMode(SwipeLayout.ShowMode mode) {
        this.mShowMode = mode;
        this.requestLayout();
    }

    public SwipeLayout.DragEdge getDragEdge() {
        return this.mCurrentDragEdge;
    }

    public int getDragDistance() {
        return this.mDragDistance;
    }

    public SwipeLayout.ShowMode getShowMode() {
        return this.mShowMode;
    }

    public View getSurfaceView() {
        return this.getChildCount() == 0 ? null : this.getChildAt(this.getChildCount() - 1);
    }

    @Nullable
    public View getCurrentBottomView() {
        List<View> bottoms = this.getBottomViews();
        return this.mCurrentDragEdge.ordinal() < bottoms.size() ? (View)bottoms.get(this.mCurrentDragEdge.ordinal()) : null;
    }

    public List<View> getBottomViews() {
        ArrayList<View> bottoms = new ArrayList();
        SwipeLayout.DragEdge[] var2 = SwipeLayout.DragEdge.values();
        int var3 = var2.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            SwipeLayout.DragEdge dragEdge = var2[var4];
            bottoms.add(this.mDragEdges.get(dragEdge));
        }

        return bottoms;
    }

    public SwipeLayout.Status getOpenStatus() {
        View surfaceView = this.getSurfaceView();
        if (surfaceView == null) {
            return SwipeLayout.Status.Close;
        } else {
            int surfaceLeft = surfaceView.getLeft();
            int surfaceTop = surfaceView.getTop();
            if (surfaceLeft == this.getPaddingLeft() && surfaceTop == this.getPaddingTop()) {
                return SwipeLayout.Status.Close;
            } else {
                return surfaceLeft != this.getPaddingLeft() - this.mDragDistance && surfaceLeft != this.getPaddingLeft() + this.mDragDistance && surfaceTop != this.getPaddingTop() - this.mDragDistance && surfaceTop != this.getPaddingTop() + this.mDragDistance ? SwipeLayout.Status.Middle : SwipeLayout.Status.Open;
            }
        }
    }

    protected void processHandRelease(float xvel, float yvel, boolean isCloseBeforeDragged) {
        float minVelocity = this.mDragHelper.getMinVelocity();
        View surfaceView = this.getSurfaceView();
        SwipeLayout.DragEdge currentDragEdge = this.mCurrentDragEdge;
        if (currentDragEdge != null && surfaceView != null) {
            float willOpenPercent = isCloseBeforeDragged ? 0.25F : 0.75F;
            float openPercent;
            if (currentDragEdge == SwipeLayout.DragEdge.Left) {
                if (xvel > minVelocity) {
                    this.open();
                } else if (xvel < -minVelocity) {
                    this.close();
                } else {
                    openPercent = 1.0F * (float)this.getSurfaceView().getLeft() / (float)this.mDragDistance;
                    if (openPercent > willOpenPercent) {
                        this.open();
                    } else {
                        this.close();
                    }
                }
            } else if (currentDragEdge == SwipeLayout.DragEdge.Right) {
                if (xvel > minVelocity) {
                    this.close();
                } else if (xvel < -minVelocity) {
                    this.open();
                } else {
                    openPercent = 1.0F * (float)(-this.getSurfaceView().getLeft()) / (float)this.mDragDistance;
                    if (openPercent > willOpenPercent) {
                        this.open();
                    } else {
                        this.close();
                    }
                }
            } else if (currentDragEdge == SwipeLayout.DragEdge.Top) {
                if (yvel > minVelocity) {
                    this.open();
                } else if (yvel < -minVelocity) {
                    this.close();
                } else {
                    openPercent = 1.0F * (float)this.getSurfaceView().getTop() / (float)this.mDragDistance;
                    if (openPercent > willOpenPercent) {
                        this.open();
                    } else {
                        this.close();
                    }
                }
            } else if (currentDragEdge == SwipeLayout.DragEdge.Bottom) {
                if (yvel > minVelocity) {
                    this.close();
                } else if (yvel < -minVelocity) {
                    this.open();
                } else {
                    openPercent = 1.0F * (float)(-this.getSurfaceView().getTop()) / (float)this.mDragDistance;
                    if (openPercent > willOpenPercent) {
                        this.open();
                    } else {
                        this.close();
                    }
                }
            }

        }
    }

    public void open() {
        this.open(true, true);
    }

    public void open(boolean smooth) {
        this.open(smooth, true);
    }

    public void open(boolean smooth, boolean notify) {
        View surface = this.getSurfaceView();
        View bottom = this.getCurrentBottomView();
        if (surface != null) {
            Rect rect = this.computeSurfaceLayoutArea(true);
            if (smooth) {
                this.mDragHelper.smoothSlideViewTo(surface, rect.left, rect.top);
            } else {
                int dx = rect.left - surface.getLeft();
                int dy = rect.top - surface.getTop();
                surface.layout(rect.left, rect.top, rect.right, rect.bottom);
                if (this.getShowMode() == SwipeLayout.ShowMode.PullOut) {
                    Rect bRect = this.computeBottomLayoutAreaViaSurface(SwipeLayout.ShowMode.PullOut, rect);
                    if (bottom != null) {
                        bottom.layout(bRect.left, bRect.top, bRect.right, bRect.bottom);
                    }
                }

                if (notify) {
                    this.dispatchRevealEvent(rect.left, rect.top, rect.right, rect.bottom);
                    this.dispatchSwipeEvent(rect.left, rect.top, dx, dy);
                } else {
                    this.safeBottomView();
                }
            }

            this.invalidate();
        }
    }

    public void open(SwipeLayout.DragEdge edge) {
        this.setCurrentDragEdge(edge);
        this.open(true, true);
    }

    public void open(boolean smooth, SwipeLayout.DragEdge edge) {
        this.setCurrentDragEdge(edge);
        this.open(smooth, true);
    }

    public void open(boolean smooth, boolean notify, SwipeLayout.DragEdge edge) {
        this.setCurrentDragEdge(edge);
        this.open(smooth, notify);
    }

    public void close() {
        this.close(true, true);
    }

    public void close(boolean smooth) {
        this.close(smooth, true);
    }

    public void close(boolean smooth, boolean notify) {
        View surface = this.getSurfaceView();
        if (surface != null) {
            if (smooth) {
                this.mDragHelper.smoothSlideViewTo(this.getSurfaceView(), this.getPaddingLeft(), this.getPaddingTop());
            } else {
                Rect rect = this.computeSurfaceLayoutArea(false);
                int dx = rect.left - surface.getLeft();
                int dy = rect.top - surface.getTop();
                surface.layout(rect.left, rect.top, rect.right, rect.bottom);
                if (notify) {
                    this.dispatchRevealEvent(rect.left, rect.top, rect.right, rect.bottom);
                    this.dispatchSwipeEvent(rect.left, rect.top, dx, dy);
                } else {
                    this.safeBottomView();
                }
            }

            this.invalidate();
        }
    }

    public void toggle() {
        this.toggle(true);
    }

    public void toggle(boolean smooth) {
        if (this.getOpenStatus() == SwipeLayout.Status.Open) {
            this.close(smooth);
        } else if (this.getOpenStatus() == SwipeLayout.Status.Close) {
            this.open(smooth);
        }

    }

    private Rect computeSurfaceLayoutArea(boolean open) {
        int l = this.getPaddingLeft();
        int t = this.getPaddingTop();
        if (open) {
            if (this.mCurrentDragEdge == SwipeLayout.DragEdge.Left) {
                l = this.getPaddingLeft() + this.mDragDistance;
            } else if (this.mCurrentDragEdge == SwipeLayout.DragEdge.Right) {
                l = this.getPaddingLeft() - this.mDragDistance;
            } else if (this.mCurrentDragEdge == SwipeLayout.DragEdge.Top) {
                t = this.getPaddingTop() + this.mDragDistance;
            } else {
                t = this.getPaddingTop() - this.mDragDistance;
            }
        }

        return new Rect(l, t, l + this.getMeasuredWidth(), t + this.getMeasuredHeight());
    }

    private Rect computeBottomLayoutAreaViaSurface(SwipeLayout.ShowMode mode, Rect surfaceArea) {
        View bottomView = this.getCurrentBottomView();
        int bl = surfaceArea.left;
        int bt = surfaceArea.top;
        int br = surfaceArea.right;
        int bb = surfaceArea.bottom;
        if (mode == SwipeLayout.ShowMode.PullOut) {
            if (this.mCurrentDragEdge == SwipeLayout.DragEdge.Left) {
                bl = surfaceArea.left - this.mDragDistance;
            } else if (this.mCurrentDragEdge == SwipeLayout.DragEdge.Right) {
                bl = surfaceArea.right;
            } else if (this.mCurrentDragEdge == SwipeLayout.DragEdge.Top) {
                bt = surfaceArea.top - this.mDragDistance;
            } else {
                bt = surfaceArea.bottom;
            }

            if (this.mCurrentDragEdge != SwipeLayout.DragEdge.Left && this.mCurrentDragEdge != SwipeLayout.DragEdge.Right) {
                bb = bt + (bottomView == null ? 0 : bottomView.getMeasuredHeight());
                br = surfaceArea.right;
            } else {
                bb = surfaceArea.bottom;
                br = bl + (bottomView == null ? 0 : bottomView.getMeasuredWidth());
            }
        } else if (mode == SwipeLayout.ShowMode.LayDown) {
            if (this.mCurrentDragEdge == SwipeLayout.DragEdge.Left) {
                br = bl + this.mDragDistance;
            } else if (this.mCurrentDragEdge == SwipeLayout.DragEdge.Right) {
                bl = br - this.mDragDistance;
            } else if (this.mCurrentDragEdge == SwipeLayout.DragEdge.Top) {
                bb = bt + this.mDragDistance;
            } else {
                bt = bb - this.mDragDistance;
            }
        }

        return new Rect(bl, bt, br, bb);
    }

    private Rect computeBottomLayDown(SwipeLayout.DragEdge dragEdge) {
        int bl = this.getPaddingLeft();
        int bt = this.getPaddingTop();
        if (dragEdge == SwipeLayout.DragEdge.Right) {
            bl = this.getMeasuredWidth() - this.mDragDistance;
        } else if (dragEdge == SwipeLayout.DragEdge.Bottom) {
            bt = this.getMeasuredHeight() - this.mDragDistance;
        }

        int br;
        int bb;
        if (dragEdge != SwipeLayout.DragEdge.Left && dragEdge != SwipeLayout.DragEdge.Right) {
            br = bl + this.getMeasuredWidth();
            bb = bt + this.mDragDistance;
        } else {
            br = bl + this.mDragDistance;
            bb = bt + this.getMeasuredHeight();
        }

        return new Rect(bl, bt, br, bb);
    }

    public void setOnDoubleClickListener(SwipeLayout.DoubleClickListener doubleClickListener) {
        this.mDoubleClickListener = doubleClickListener;
    }

    private int dp2px(float dp) {
        return (int)(dp * this.getContext().getResources().getDisplayMetrics().density + 0.5F);
    }

    /** @deprecated */
    @Deprecated
    public void setDragEdge(SwipeLayout.DragEdge dragEdge) {
        if (this.getChildCount() >= 2) {
            this.mDragEdges.put(dragEdge, this.getChildAt(this.getChildCount() - 2));
        }

        this.setCurrentDragEdge(dragEdge);
    }

    public void onViewRemoved(View child) {
        Iterator var2 = (new HashMap(this.mDragEdges)).entrySet().iterator();

        while(var2.hasNext()) {
            Entry<DragEdge, View> entry = (Entry)var2.next();
            if (entry.getValue() == child) {
                this.mDragEdges.remove(entry.getKey());
            }
        }

    }

    public Map<DragEdge, View> getDragEdgeMap() {
        return this.mDragEdges;
    }

    /** @deprecated */
    @Deprecated
    public List<DragEdge> getDragEdges() {
        return new ArrayList(this.mDragEdges.keySet());
    }

    /** @deprecated */
    @Deprecated
    public void setDragEdges(List<DragEdge> dragEdges) {
        int i = 0;

        for(int size = Math.min(dragEdges.size(), this.getChildCount() - 1); i < size; ++i) {
            SwipeLayout.DragEdge dragEdge = (SwipeLayout.DragEdge)dragEdges.get(i);
            this.mDragEdges.put(dragEdge, this.getChildAt(i));
        }

        if (dragEdges.size() != 0 && !dragEdges.contains(DefaultDragEdge)) {
            this.setCurrentDragEdge((SwipeLayout.DragEdge)dragEdges.get(0));
        } else {
            this.setCurrentDragEdge(DefaultDragEdge);
        }

    }

    /** @deprecated */
    @Deprecated
    public void setDragEdges(SwipeLayout.DragEdge... mDragEdges) {
        this.setDragEdges(Arrays.asList(mDragEdges));
    }

    /** @deprecated */
    @Deprecated
    public void setBottomViewIds(int leftId, int rightId, int topId, int bottomId) {
        this.addDrag(SwipeLayout.DragEdge.Left, this.findViewById(leftId));
        this.addDrag(SwipeLayout.DragEdge.Right, this.findViewById(rightId));
        this.addDrag(SwipeLayout.DragEdge.Top, this.findViewById(topId));
        this.addDrag(SwipeLayout.DragEdge.Bottom, this.findViewById(bottomId));
    }

    private float getCurrentOffset() {
        return this.mCurrentDragEdge == null ? 0.0F : this.mEdgeSwipesOffset[this.mCurrentDragEdge.ordinal()];
    }

    private void setCurrentDragEdge(SwipeLayout.DragEdge dragEdge) {
        if (this.mCurrentDragEdge != dragEdge) {
            this.mCurrentDragEdge = dragEdge;
            this.updateBottomViews();
        }

    }

    private void updateBottomViews() {
        View currentBottomView = this.getCurrentBottomView();
        if (currentBottomView != null) {
            if (this.mCurrentDragEdge != SwipeLayout.DragEdge.Left && this.mCurrentDragEdge != SwipeLayout.DragEdge.Right) {
                this.mDragDistance = currentBottomView.getMeasuredHeight() - this.dp2px(this.getCurrentOffset());
            } else {
                this.mDragDistance = currentBottomView.getMeasuredWidth() - this.dp2px(this.getCurrentOffset());
            }
        }

        if (this.mShowMode == SwipeLayout.ShowMode.PullOut) {
            this.layoutPullOut();
        } else if (this.mShowMode == SwipeLayout.ShowMode.LayDown) {
            this.layoutLayDown();
        }

        this.safeBottomView();
    }

    static {
        DefaultDragEdge = SwipeLayout.DragEdge.Right;
    }

    public interface DoubleClickListener {
        void onDoubleClick(SwipeLayout var1, boolean var2);
    }

    public static enum Status {
        Middle,
        Open,
        Close;

        private Status() {
        }
    }

    class SwipeDetector extends SimpleOnGestureListener {
        SwipeDetector() {
        }

        public boolean onSingleTapUp(MotionEvent e) {
            if (SwipeLayout.this.mClickToClose && SwipeLayout.this.isTouchOnSurface(e)) {
                SwipeLayout.this.close();
            }

            return super.onSingleTapUp(e);
        }

        public boolean onDoubleTap(MotionEvent e) {
            if (SwipeLayout.this.mDoubleClickListener != null) {
                View bottom = SwipeLayout.this.getCurrentBottomView();
                View surface = SwipeLayout.this.getSurfaceView();
                View target;
                if (bottom != null && e.getX() > (float)bottom.getLeft() && e.getX() < (float)bottom.getRight() && e.getY() > (float)bottom.getTop() && e.getY() < (float)bottom.getBottom()) {
                    target = bottom;
                } else {
                    target = surface;
                }

                SwipeLayout.this.mDoubleClickListener.onDoubleClick(SwipeLayout.this, target == surface);
            }

            return true;
        }
    }

    public interface OnLayout {
        void onLayout(SwipeLayout var1);
    }

    public interface OnRevealListener {
        void onReveal(View var1, SwipeLayout.DragEdge var2, float var3, int var4);
    }

    public interface SwipeDenier {
        boolean shouldDenySwipe(MotionEvent var1);
    }

    public interface SwipeListener {
        void onStartOpen(SwipeLayout var1);

        void onOpen(SwipeLayout var1);

        void onStartClose(SwipeLayout var1);

        void onClose(SwipeLayout var1);

        void onUpdate(SwipeLayout var1, int var2, int var3);

        void onHandRelease(SwipeLayout var1, float var2, float var3);
    }

    public static enum ShowMode {
        LayDown,
        PullOut;

        private ShowMode() {
        }
    }

    public static enum DragEdge {
        Left,
        Top,
        Right,
        Bottom;

        private DragEdge() {
        }
    }
}
