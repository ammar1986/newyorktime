/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.tf961.newyorktimes.data.remote;

import com.google.gson.JsonObject;
import com.tf961.newyorktimes.data.model.api.BaseResponse;
import com.tf961.newyorktimes.data.model.api.mainPage.NYTimeMockObject;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;

/**
 * Created by assaad on 07/07/17.
 */

public interface ApiHelper {

    ApiHeader getApiHeader();


    Single<BaseResponse<ArrayList<NYTimeMockObject>>> getArticles(int period);
}
