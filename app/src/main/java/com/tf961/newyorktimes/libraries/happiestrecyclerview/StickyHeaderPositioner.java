//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static android.view.View.VISIBLE;

final class StickyHeaderPositioner {
    static final int NO_ELEVATION = -1;
    static final int DEFAULT_ELEVATION = 5;
    private static final int INVALID_POSITION = -1;
    private final RecyclerView recyclerView;
    private final boolean checkMargins;
    private View currentHeader;
    private int lastBoundPosition = -1;
    private List<Integer> headerPositions;
    private int orientation;
    private boolean dirty;
    private float headerElevation = -1.0F;
    private int cachedElevation = -1;
    private RecyclerView.ViewHolder currentViewHolder;
    @Nullable
    private StickyHeaderListener listener;

    StickyHeaderPositioner(androidx.recyclerview.widget.RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                int visibility = StickyHeaderPositioner.this.recyclerView.getVisibility();
                if (StickyHeaderPositioner.this.currentHeader != null) {
                    StickyHeaderPositioner.this.currentHeader.setVisibility(visibility);
                }

            }
        });
        this.checkMargins = this.recyclerViewHasPadding();
    }

    void setHeaderPositions(List<Integer> headerPositions) {
        this.headerPositions = headerPositions;
    }

    void updateHeaderState(int firstVisiblePosition, Map<Integer, View> visibleHeaders, ViewRetriever viewRetriever, boolean atTop) {
        int headerPositionToShow = atTop ? -1 : this.getHeaderPositionToShow(firstVisiblePosition, (View)visibleHeaders.get(firstVisiblePosition));
        View headerToCopy = (View)visibleHeaders.get(headerPositionToShow);
        if (headerPositionToShow != this.lastBoundPosition) {
            if (headerPositionToShow != -1 && (!this.checkMargins || !this.headerAwayFromEdge(headerToCopy))) {
                this.lastBoundPosition = headerPositionToShow;
                RecyclerView.ViewHolder viewHolder = viewRetriever.getViewHolderForPosition(headerPositionToShow);
                this.attachHeader(viewHolder, headerPositionToShow);
            } else {
                this.dirty = true;
                this.safeDetachHeader();
                this.lastBoundPosition = -1;
            }
        } else if (this.checkMargins && this.headerAwayFromEdge(headerToCopy)) {
            this.detachHeader(this.lastBoundPosition);
            this.lastBoundPosition = -1;
        }

        this.checkHeaderPositions(visibleHeaders);
        this.recyclerView.post(new Runnable() {
            public void run() {
                StickyHeaderPositioner.this.checkElevation();
            }
        });
    }

    void checkHeaderPositions(Map<Integer, View> visibleHeaders) {
        if (this.currentHeader != null) {
            if (this.currentHeader.getHeight() == 0) {
                this.waitForLayoutAndRetry(visibleHeaders);
            } else {
                boolean reset = true;
                Iterator var3 = visibleHeaders.entrySet().iterator();

                while(var3.hasNext()) {
                    Entry<Integer, View> entry = (Entry)var3.next();
                    if ((Integer)entry.getKey() > this.lastBoundPosition) {
                        View nextHeader = (View)entry.getValue();
                        reset = this.offsetHeader(nextHeader) == -1.0F;
                        break;
                    }
                }

                if (reset) {
                    this.resetTranslation();
                }

                this.currentHeader.setVisibility(VISIBLE);
            }
        }
    }

    void setElevateHeaders(int dpElevation) {
        if (dpElevation != -1) {
            this.cachedElevation = dpElevation;
        } else {
            this.headerElevation = -1.0F;
            this.cachedElevation = -1;
        }

    }

    void reset(int orientation) {
        this.orientation = orientation;
        this.lastBoundPosition = -1;
        this.dirty = true;
        this.safeDetachHeader();
    }

    void clearHeader() {
        this.detachHeader(this.lastBoundPosition);
    }

    void setListener(@Nullable StickyHeaderListener listener) {
        this.listener = listener;
    }

    private float offsetHeader(View nextHeader) {
        boolean shouldOffsetHeader = this.shouldOffsetHeader(nextHeader);
        float offset = -1.0F;
        if (shouldOffsetHeader) {
            if (this.orientation == 1) {
                offset = -((float)this.currentHeader.getHeight() - nextHeader.getY());
                this.currentHeader.setTranslationY(offset);
            } else {
                offset = -((float)this.currentHeader.getWidth() - nextHeader.getX());
                this.currentHeader.setTranslationX(offset);
            }
        }

        return offset;
    }

    private boolean shouldOffsetHeader(View nextHeader) {
        if (this.orientation == 1) {
            return nextHeader.getY() < (float)this.currentHeader.getHeight();
        } else {
            return nextHeader.getX() < (float)this.currentHeader.getWidth();
        }
    }

    private void resetTranslation() {
        if (this.orientation == 1) {
            this.currentHeader.setTranslationY(0.0F);
        } else {
            this.currentHeader.setTranslationX(0.0F);
        }

    }

    private int getHeaderPositionToShow(int firstVisiblePosition, @Nullable View headerForPosition) {
        int headerPositionToShow = -1;
        if (this.headerIsOffset(headerForPosition)) {
            int offsetHeaderIndex = this.headerPositions.indexOf(firstVisiblePosition);
            if (offsetHeaderIndex > 0) {
                return (Integer)this.headerPositions.get(offsetHeaderIndex - 1);
            }
        }

        Integer headerPosition;
        for(Iterator var6 = this.headerPositions.iterator(); var6.hasNext(); headerPositionToShow = headerPosition) {
            headerPosition = (Integer)var6.next();
            if (headerPosition > firstVisiblePosition) {
                break;
            }
        }

        return headerPositionToShow;
    }

    private boolean headerIsOffset(View headerForPosition) {
        if (headerForPosition != null) {
            return this.orientation == 1 ? headerForPosition.getY() > 0.0F : headerForPosition.getX() > 0.0F;
        } else {
            return false;
        }
    }

    @VisibleForTesting
    void attachHeader(RecyclerView.ViewHolder viewHolder, int headerPosition) {
        if (viewHolder != null) {
            if (this.currentViewHolder == viewHolder) {
                this.callDetach(this.lastBoundPosition);
                this.recyclerView.getAdapter().onBindViewHolder(this.currentViewHolder, headerPosition);
                this.currentViewHolder.itemView.requestLayout();
                this.checkTranslation();
                this.callAttach(headerPosition);
                this.dirty = false;
            } else {
                this.detachHeader(this.lastBoundPosition);
                this.currentViewHolder = viewHolder;
                this.recyclerView.getAdapter().onBindViewHolder(this.currentViewHolder, headerPosition);
                this.currentHeader = this.currentViewHolder.itemView;
                this.callAttach(headerPosition);
                this.resolveElevationSettings(this.currentHeader.getContext());
                this.currentHeader.setVisibility(View.INVISIBLE);
                this.currentHeader.setId(0);
                this.getRecyclerParent().addView(this.currentHeader);
                if (this.checkMargins) {
                    this.updateLayoutParams(this.currentHeader);
                }

                this.dirty = false;
            }
        }
    }

    private int currentDimension() {
        if (this.currentHeader == null) {
            return 0;
        } else {
            return this.orientation == 1 ? this.currentHeader.getHeight() : this.currentHeader.getWidth();
        }
    }

    private boolean headerHasTranslation() {
        if (this.currentHeader == null) {
            return false;
        } else if (this.orientation == 1) {
            return this.currentHeader.getTranslationY() < 0.0F;
        } else {
            return this.currentHeader.getTranslationX() < 0.0F;
        }
    }

    private void updateTranslation(int diff) {
        if (this.currentHeader != null) {
            if (this.orientation == 1) {
                this.currentHeader.setTranslationY(this.currentHeader.getTranslationY() + (float)diff);
            } else {
                this.currentHeader.setTranslationX(this.currentHeader.getTranslationX() + (float)diff);
            }

        }
    }

    private void checkTranslation() {
        final View view = this.currentHeader;
        if (view != null) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                int previous = StickyHeaderPositioner.this.currentDimension();

                public void onGlobalLayout() {
                    if (VERSION.SDK_INT >= 16) {
                        view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }

                    if (StickyHeaderPositioner.this.currentHeader != null) {
                        int newDimen = StickyHeaderPositioner.this.currentDimension();
                        if (StickyHeaderPositioner.this.headerHasTranslation() && this.previous != newDimen) {
                            StickyHeaderPositioner.this.updateTranslation(this.previous - newDimen);
                        }

                    }
                }
            });
        }
    }

    private void checkElevation() {
        if (this.headerElevation != -1.0F && this.currentHeader != null) {
            if ((this.orientation != 1 || this.currentHeader.getTranslationY() != 0.0F) && (this.orientation != 0 || this.currentHeader.getTranslationX() != 0.0F)) {
                this.settleHeader();
            } else {
                this.elevateHeader();
            }
        }

    }

    private void elevateHeader() {
        if (VERSION.SDK_INT >= 21) {
            if (this.currentHeader.getTag() != null) {
                return;
            }

            this.currentHeader.setTag(true);
            this.currentHeader.animate().z(this.headerElevation);
        }

    }

    private void settleHeader() {
        if (VERSION.SDK_INT >= 21 && this.currentHeader.getTag() != null) {
            this.currentHeader.setTag((Object)null);
            this.currentHeader.animate().z(0.0F);
        }

    }

    private void detachHeader(int position) {
        if (this.currentHeader != null) {
            this.getRecyclerParent().removeView(this.currentHeader);
            this.callDetach(position);
            this.currentHeader = null;
            this.currentViewHolder = null;
        }

    }

    private void callAttach(int position) {
        if (this.listener != null) {
            this.listener.headerAttached(this.currentHeader, position);
        }

    }

    private void callDetach(int position) {
        if (this.listener != null) {
            this.listener.headerDetached(this.currentHeader, position);
        }

    }

    private void updateLayoutParams(View currentHeader) {
        MarginLayoutParams params = (MarginLayoutParams)currentHeader.getLayoutParams();
        this.matchMarginsToPadding(params);
    }

    private void matchMarginsToPadding(MarginLayoutParams layoutParams) {
        int leftMargin = this.orientation == 1 ? this.recyclerView.getPaddingLeft() : 0;
        int topMargin = this.orientation == 1 ? 0 : this.recyclerView.getPaddingTop();
        int rightMargin = this.orientation == 1 ? this.recyclerView.getPaddingRight() : 0;
        layoutParams.setMargins(leftMargin, topMargin, rightMargin, 0);
    }

    private boolean headerAwayFromEdge(View headerToCopy) {
        if (headerToCopy != null) {
            return this.orientation == 1 ? headerToCopy.getY() > 0.0F : headerToCopy.getX() > 0.0F;
        } else {
            return false;
        }
    }

    private boolean recyclerViewHasPadding() {
        return this.recyclerView.getPaddingLeft() > 0 || this.recyclerView.getPaddingRight() > 0 || this.recyclerView.getPaddingTop() > 0;
    }

    private ViewGroup getRecyclerParent() {
        return (ViewGroup)this.recyclerView.getParent();
    }

    private void waitForLayoutAndRetry(final Map<Integer, View> visibleHeaders) {
        final View view = this.currentHeader;
        if (view != null) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    if (VERSION.SDK_INT >= 16) {
                        view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }

                    if (StickyHeaderPositioner.this.currentHeader != null) {
                        StickyHeaderPositioner.this.getRecyclerParent().requestLayout();
                        StickyHeaderPositioner.this.checkHeaderPositions(visibleHeaders);
                    }
                }
            });
        }
    }

    private void safeDetachHeader() {
        final int cachedPosition = this.lastBoundPosition;
        this.getRecyclerParent().post(new Runnable() {
            public void run() {
                if (StickyHeaderPositioner.this.dirty) {
                    StickyHeaderPositioner.this.detachHeader(cachedPosition);
                }

            }
        });
    }

    @VisibleForTesting
    int getLastBoundPosition() {
        return this.lastBoundPosition;
    }

    private void resolveElevationSettings(Context context) {
        if (this.cachedElevation != -1 && this.headerElevation == -1.0F) {
            this.headerElevation = this.pxFromDp(context, this.cachedElevation);
        }

    }

    private float pxFromDp(Context context, int dp) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (float)dp * scale;
    }
}
