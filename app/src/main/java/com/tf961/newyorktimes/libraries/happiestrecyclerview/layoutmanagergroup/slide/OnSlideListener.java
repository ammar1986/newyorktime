//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview.layoutmanagergroup.slide;

import androidx.recyclerview.widget.RecyclerView.ViewHolder;

public interface OnSlideListener<T> {
    void onSliding(ViewHolder var1, float var2, int var3);

    void onSlided(ViewHolder var1, T var2, int var3);

    void onClear();
}
