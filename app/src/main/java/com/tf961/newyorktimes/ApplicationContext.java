package com.tf961.newyorktimes;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import androidx.databinding.BindingAdapter;
import androidx.lifecycle.LifecycleObserver;

import com.androidnetworking.AndroidNetworking;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.decoder.SimpleProgressiveJpegConfig;
import com.tf961.newyorktimes.utils.AppLogger;

import okhttp3.OkHttpClient;

public class ApplicationContext extends Application implements LifecycleObserver {

    static ApplicationContext applicationContext;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = this;
//        Fresco.initialize(this);
        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setProgressiveJpegConfig(new SimpleProgressiveJpegConfig())
                .setDownsampleEnabled(true).setResizeAndRotateEnabledForNetwork(true)
                .build();
        Fresco.initialize(this, config);
        AppLogger.init();

        OkHttpClient client=new OkHttpClient();
        AndroidNetworking.initialize(this, client);
        if (BuildConfig.DEBUG) {
        AndroidNetworking.enableLogging();
        }

    }

    @BindingAdapter("actualImageUri")
    public static void setImageUri(SimpleDraweeView view, String url) {
        if (!TextUtils.isEmpty(url)) {
            view.setImageURI(Uri.parse(url));
        }

    }

    public static ApplicationContext getInstance() {
        return applicationContext;
    }


}
