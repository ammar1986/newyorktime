//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

public interface OnItemClickedListener {
    void onItemClicked(int var1);

    void onItemLongClicked(int var1);
}
