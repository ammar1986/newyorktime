//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.HappyDB.encryption;

import java.io.ByteArrayOutputStream;

public class b64 {
    private static char[] map1 = new char[64];
    private static final String base64code = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    private static final int splitLinesAt = 76;

    public b64() {
    }

    public static byte[] zeroPad(int length, byte[] bytes) {
        byte[] padded = new byte[length];
        System.arraycopy(bytes, 0, padded, 0, bytes.length);
        return padded;
    }

    public static String encode(String string) {
        String encoded = "";

        byte[] stringArray;
        try {
            stringArray = string.getBytes("UTF-8");
        } catch (Exception var6) {
            stringArray = string.getBytes();
        }

        int paddingCount = (3 - stringArray.length % 3) % 3;
        stringArray = zeroPad(stringArray.length + paddingCount, stringArray);

        for(int i = 0; i < stringArray.length; i += 3) {
            int j = ((stringArray[i] & 255) << 16) + ((stringArray[i + 1] & 255) << 8) + (stringArray[i + 2] & 255);
            encoded = encoded + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(j >> 18 & 63) + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(j >> 12 & 63) + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(j >> 6 & 63) + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(j & 63);
        }

        return splitLines(encoded.substring(0, encoded.length() - paddingCount) + "==".substring(0, paddingCount));
    }

    public static String splitLines(String string) {
        String lines = "";

        for(int i = 0; i < string.length(); i += 76) {
            lines = lines + string.substring(i, Math.min(string.length(), i + 76));
            lines = lines + "\r\n";
        }

        return lines;
    }

    public static String encode1(byte[] in) {
        int iLen = in.length;
        int oDataLen = (iLen * 4 + 2) / 3;
        int oLen = (iLen + 2) / 3 * 4;
        char[] out = new char[oLen];
        int ip = 0;

        for(int op = 0; ip < iLen; ++op) {
            int i0 = in[ip++] & 255;
            int i1 = ip < iLen ? in[ip++] & 255 : 0;
            int i2 = ip < iLen ? in[ip++] & 255 : 0;
            int o0 = i0 >>> 2;
            int o1 = (i0 & 3) << 4 | i1 >>> 4;
            int o2 = (i1 & 15) << 2 | i2 >>> 6;
            int o3 = i2 & 63;
            out[op++] = map1[o0];
            out[op++] = map1[o1];
            out[op] = op < oDataLen ? map1[o2] : 61;
            ++op;
            out[op] = op < oDataLen ? map1[o3] : 61;
        }

        return new String(out);
    }

    public static byte[] decode1(String s) {
        int i = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int len = s.length();

        while(true) {
            while(i < len && s.charAt(i) <= ' ') {
                ++i;
            }

            if (i == len) {
                break;
            }

            int tri = (decode(s.charAt(i)) << 18) + (decode(s.charAt(i + 1)) << 12) + (decode(s.charAt(i + 2)) << 6) + decode(s.charAt(i + 3));
            bos.write(tri >> 16 & 255);
            if (s.charAt(i + 2) == '=') {
                break;
            }

            bos.write(tri >> 8 & 255);
            if (s.charAt(i + 3) == '=') {
                break;
            }

            bos.write(tri & 255);
            i += 4;
        }

        return bos.toByteArray();
    }

    public static String base64Encode(byte[] in) {
        int iLen = in.length;
        int oDataLen = (iLen * 4 + 2) / 3;
        int oLen = (iLen + 2) / 3 * 4;
        char[] out = new char[oLen];
        int ip = 0;

        for(int op = 0; ip < iLen; ++op) {
            int i0 = in[ip++] & 255;
            int i1 = ip < iLen ? in[ip++] & 255 : 0;
            int i2 = ip < iLen ? in[ip++] & 255 : 0;
            int o0 = i0 >>> 2;
            int o1 = (i0 & 3) << 4 | i1 >>> 4;
            int o2 = (i1 & 15) << 2 | i2 >>> 6;
            int o3 = i2 & 63;
            out[op++] = map1[o0];
            out[op++] = map1[o1];
            out[op] = op < oDataLen ? map1[o2] : 61;
            ++op;
            out[op] = op < oDataLen ? map1[o3] : 61;
        }

        return new String(out);
    }

    public static byte[] Base64Decode(String s) {
        int i = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int len = s.length();

        while(true) {
            while(i < len && s.charAt(i) <= ' ') {
                ++i;
            }

            if (i == len) {
                break;
            }

            int tri = (decode(s.charAt(i)) << 18) + (decode(s.charAt(i + 1)) << 12) + (decode(s.charAt(i + 2)) << 6) + decode(s.charAt(i + 3));
            bos.write(tri >> 16 & 255);
            if (s.charAt(i + 2) == '=') {
                break;
            }

            bos.write(tri >> 8 & 255);
            if (s.charAt(i + 3) == '=') {
                break;
            }

            bos.write(tri & 255);
            i += 4;
        }

        return bos.toByteArray();
    }

    private static int decode(char c) {
        if (c >= 'A' && c <= 'Z') {
            return c - 65;
        } else if (c >= 'a' && c <= 'z') {
            return c - 97 + 26;
        } else if (c >= '0' && c <= '9') {
            return c - 48 + 26 + 26;
        } else {
            switch(c) {
            case '+':
                return 62;
            case '/':
                return 63;
            case '=':
                return 0;
            default:
                throw new RuntimeException("unexpected code: " + c);
            }
        }
    }

    static {
        int i = 0;

        char c;
        for(c = 'A'; c <= 'Z'; map1[i++] = c++) {
        }

        for(c = 'a'; c <= 'z'; map1[i++] = c++) {
        }

        for(c = '0'; c <= '9'; map1[i++] = c++) {
        }

        map1[i++] = '+';
        map1[i++] = '/';
    }
}
