//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview.layoutmanagergroup.slide;

import android.graphics.Canvas;
import android.view.View;
import android.view.View.OnTouchListener;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper.Callback;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.recyclerview.widget.RecyclerView.LayoutManager;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import java.util.List;

public class ItemTouchHelperCallback<T> extends Callback {
    private final Adapter adapter;
    private List<T> dataList;
    private OnSlideListener<T> mListener;

    public ItemTouchHelperCallback(@NonNull Adapter adapter, @NonNull List<T> dataList) {
        this.adapter = (Adapter)this.checkIsNull(adapter);
        this.dataList = (List)this.checkIsNull(dataList);
    }

    public ItemTouchHelperCallback(@NonNull Adapter adapter, @NonNull List<T> dataList, OnSlideListener<T> listener) {
        this.adapter = (Adapter)this.checkIsNull(adapter);
        this.dataList = (List)this.checkIsNull(dataList);
        this.mListener = listener;
    }

    private <T> T checkIsNull(T t) {
        if (t == null) {
            throw new NullPointerException();
        } else {
            return t;
        }
    }

    public void setOnSlideListener(OnSlideListener<T> mListener) {
        this.mListener = mListener;
    }

    public int getMovementFlags(RecyclerView recyclerView, ViewHolder viewHolder) {
        int dragFlags = 0;
        int slideFlags = 0;
        LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof LayoutManagerSlide) {
            slideFlags = 12;
        }

        return makeMovementFlags(dragFlags, slideFlags);
    }

    public boolean onMove(RecyclerView recyclerView, ViewHolder viewHolder, ViewHolder target) {
        return false;
    }

    public void onSwiped(ViewHolder viewHolder, int direction) {
        viewHolder.itemView.setOnTouchListener((OnTouchListener)null);
        int layoutPosition = viewHolder.getLayoutPosition();
        T remove = this.dataList.remove(layoutPosition);
        this.adapter.notifyDataSetChanged();
        if (this.mListener != null) {
            this.mListener.onSlided(viewHolder, remove, direction == 4 ? 1 : 4);
        }

        if (this.adapter.getItemCount() == 0 && this.mListener != null) {
            this.mListener.onClear();
        }

    }

    public boolean isItemViewSwipeEnabled() {
        return false;
    }

    public void onChildDraw(Canvas c, RecyclerView recyclerView, ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        View itemView = viewHolder.itemView;
        if (actionState == 1) {
            float ratio = dX / this.getThreshold(recyclerView, viewHolder);
            if (ratio > 1.0F) {
                ratio = 1.0F;
            } else if (ratio < -1.0F) {
                ratio = -1.0F;
            }

            itemView.setRotation(ratio * 15.0F);
            int childCount = recyclerView.getChildCount();
            int position;
            int index;
            View view;
            if (childCount > 3) {
                for(position = 1; position < childCount - 1; ++position) {
                    index = childCount - position - 1;
                    view = recyclerView.getChildAt(position);
                    view.setScaleX(1.0F - (float)index * 0.1F + Math.abs(ratio) * 0.1F);
                    view.setScaleY(1.0F - (float)index * 0.1F + Math.abs(ratio) * 0.1F);
                    view.setTranslationY(((float)index - Math.abs(ratio)) * (float)itemView.getMeasuredHeight() / 14.0F);
                }
            } else {
                for(position = 0; position < childCount - 1; ++position) {
                    index = childCount - position - 1;
                    view = recyclerView.getChildAt(position);
                    view.setScaleX(1.0F - (float)index * 0.1F + Math.abs(ratio) * 0.1F);
                    view.setScaleY(1.0F - (float)index * 0.1F + Math.abs(ratio) * 0.1F);
                    view.setTranslationY(((float)index - Math.abs(ratio)) * (float)itemView.getMeasuredHeight() / 14.0F);
                }
            }

            if (this.mListener != null) {
                if (ratio != 0.0F) {
                    this.mListener.onSliding(viewHolder, ratio, ratio < 0.0F ? 4 : 8);
                } else {
                    this.mListener.onSliding(viewHolder, ratio, 1);
                }
            }
        }

    }

    public void clearView(RecyclerView recyclerView, ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);
        viewHolder.itemView.setRotation(0.0F);
    }

    private float getThreshold(RecyclerView recyclerView, ViewHolder viewHolder) {
        return (float)recyclerView.getWidth() * this.getSwipeThreshold(viewHolder);
    }
}
