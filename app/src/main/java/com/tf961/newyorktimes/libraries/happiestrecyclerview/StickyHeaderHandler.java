//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

import android.util.SparseArray;

interface StickyHeaderHandler {
    SparseArray<String> getAdapterData();
}
