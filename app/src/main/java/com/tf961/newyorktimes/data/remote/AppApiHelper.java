/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.tf961.newyorktimes.data.remote;

import android.text.TextUtils;

import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.rx2androidnetworking.Rx2ANRequest;
import com.rx2androidnetworking.Rx2AndroidNetworking;
import com.tf961.newyorktimes.data.model.api.BaseResponse;
import com.tf961.newyorktimes.data.model.api.mainPage.NYTimeMockObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import okhttp3.OkHttpClient;

/**
 * Created by assaad on 07/07/17.
 */

public class AppApiHelper implements ApiHelper {

    private ApiHeader mApiHeader;

    private static AppApiHelper appApiHelper;

    public static AppApiHelper getInstance() {
        return appApiHelper;
    }

    public static AppApiHelper getInstance(ApiHeader apiHeader) {
        if (appApiHelper == null) {
            appApiHelper = new AppApiHelper(apiHeader);
        }
        return appApiHelper;
    }

    public AppApiHelper(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHeader;
    }


    @Override
    public Single<BaseResponse<ArrayList<NYTimeMockObject>>> getArticles(int period) {
        return Rx2AndroidNetworking.get(String.format(ApiEndPoint.ENDPOINT_GET_ARTICLES, period))
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getParseSingle(new TypeToken<BaseResponse<ArrayList<NYTimeMockObject>>>() {
                });
    }


}
