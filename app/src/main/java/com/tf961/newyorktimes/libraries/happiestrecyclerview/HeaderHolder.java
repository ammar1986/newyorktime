//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tf961.newyorktimes.libraries.happiestrecyclerview;

import android.view.View;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public abstract class HeaderHolder extends ViewHolder {
    public HeaderHolder(View itemView) {
        super(itemView);
    }

    public abstract View getMainView();

    public void setVisibility(boolean visibility) {
        if (visibility) {
            this.getMainView().setVisibility(VISIBLE);
        } else {
            this.getMainView().setVisibility(INVISIBLE);
        }

    }
}
